package com.lanetracker.lanesearcher.configuration;

import com.lanetracker.lanesearcher.rest.exceptions.InvalidTokenException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(InvalidTokenException.class)
    private ResponseEntity badRequestHandler(InvalidTokenException br) {

        HttpStatus status = HttpStatus.BAD_REQUEST;

        return new ResponseEntity<>("whoops", status);
    }

}
