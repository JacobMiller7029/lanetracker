package com.lanetracker.lanesearcher.configuration;

public class Constants {

    public static final String SEARCH_TYPE_NAME = "name";
    public static final String SEARCH_TYPE_MC = "mc#";
    public static final String SEARCH_TYPE_ODOT = "odot#";

}