package com.lanetracker.lanesearcher.configuration;

import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Configuration
public class BeanConfig {

    @Bean
    public ModelMapper getModelMapper(){
        log.info("Model Mapper configuration");
        return new ModelMapper();
    }

    @Bean
    public RestTemplate getRestTemplateBean() {
        log.info("Configuring rest template bean... -------");
        return new RestTemplate();
    }

}
