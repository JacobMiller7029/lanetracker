package com.lanetracker.lanesearcher.BootStrap_Data;


import com.lanetracker.lanesearcher.domain.Carrier;
import com.lanetracker.lanesearcher.domain.Contact;
import com.lanetracker.lanesearcher.domain.Equipment;
import com.lanetracker.lanesearcher.domain.Lane;
import com.lanetracker.lanesearcher.repository.CarrierRepository;
import com.lanetracker.lanesearcher.repository.LaneRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Slf4j
@Component
public class BootStrapData  {

    private LaneRepository laneRepository;
    private CarrierRepository carrierRepository;
    private Environment env;

    @Autowired
    public BootStrapData(LaneRepository laneRepository, CarrierRepository carrierRepository, Environment environment) {
        this.laneRepository = laneRepository;
        this.carrierRepository = carrierRepository;
        this.env = environment;
    }

    @EventListener(ContextRefreshedEvent.class)
    public void onRefresh() {
        if(Arrays.asList(env.getActiveProfiles()).contains("local")) {
            carrierRepository.save(createCarrierAndLane());
            carrierRepository.save(createSecondCarrierAndLanes());
            carrierRepository.save(createThirdCarrierAndLane());
            carrierRepository.save(createTwinFallsCarrierAndLane());
            log.info("Carrier saved to database...~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        }
    }


    public Carrier createCarrierAndLane(){

        Carrier carrier = new Carrier();
        carrier.setTitle("Bill Gulick Trucking Co");
        carrier.setNotes("This carrier should be chosen as a last resort. ");
        carrier.setHomeCity("Los Angeles, California, USA");
        carrier.setHomeLat(34.0522);
        carrier.setHomeLong(-118.2437);

        Contact contact = new Contact();
        contact.setEmail("jbourne@bulickstrucking.com");
        contact.setName("Jason Bourne");
        contact.setPhoneNum("360-435-4367");
        contact.setFaxNum("360-767-9329");

        Contact contact1 = new Contact();
        contact1.setEmail("bpitt@bulickstrucking.com");
        contact1.setName("Brad Pitt");
        contact1.setPhoneNum("360-263-8476");
        contact1.setFaxNum("360-767-9329");

        Contact contact2 = new Contact();
        contact2.setEmail("hford@bulickstrucking.com");
        contact2.setName("Harrison Ford");
        contact2.setPhoneNum("360-435-4367");
        contact2.setFaxNum("360-767-9329");

        Contact contact3 = new Contact();
        contact3.setEmail("mmcconaughey@bulickstrucking.com");
        contact3.setName("Matthew Mcconaughey");
        contact3.setPhoneNum("360-263-8476");
        contact3.setFaxNum("360-767-9329");

        Contact contact4 = new Contact();
        contact4.setEmail("rname.1@bulickstrucking.com");
        contact4.setName("Random Name 1");
        contact4.setPhoneNum("360-435-4367");
        contact4.setFaxNum("360-767-9329");

        Contact contact5 = new Contact();
        contact5.setEmail("rname.2@bulickstrucking.com");
        contact5.setName("Random Name 2");
        contact5.setPhoneNum("360-263-8476");
        contact5.setFaxNum("360-767-9329");

        Lane lane1 = new Lane();
        lane1.setFlatRate(5120);

        //Start in portland
        lane1.setStartLocationTitle("Portland, Oregon, USA");
        lane1.setStartLat(45.5231);
        lane1.setStartLong(-122.6765);

        //End in Los Angeles
        lane1.setEndLocationTitle("Los Angeles, California, USA");
        lane1.setEndLat(34.0522);
        lane1.setEndLong(-118.2437);
        lane1.setEquipmentType(Equipment.DRY_VAN);

        Lane lane2 = new Lane();
        lane2.setFlatRate(4200);

        //Start in Los Angeles
        lane2.setStartLocationTitle("Los Angeles, California, USA");
        lane2.setStartLat(34.0522);
        lane2.setStartLong(-118.2437);

        //End in Portland
        lane2.setEndLocationTitle("Portland, Oregon, USA");
        lane2.setEndLat(45.5231);
        lane2.setEndLong(-122.6765);
        lane2.setEquipmentType(Equipment.FLATBEAD);

        carrier.addLane(lane1);
        carrier.addLane(lane2);

        carrier.addContact(contact);
        carrier.addContact(contact1);

        carrier.addContact(contact2);
        carrier.addContact(contact3);

        carrier.addContact(contact4);
        carrier.addContact(contact5);


        return carrier;
    }

    private Carrier createSecondCarrierAndLanes() {

        Carrier carrier = new Carrier();
        carrier.setTitle("Jackson's Carrier service");
        carrier.setNotes("A great driver, always on time.");
        carrier.setHomeCity("Portland, Oregon, USA");
        carrier.setHomeLat(45.5231);
        carrier.setHomeLong(-122.6765);

        Contact contact = new Contact();
        contact.setEmail("Jbond@jacksoncarrier.com");
        contact.setName("James Bond");
        contact.setPhoneNum("509-378-2298");
        contact.setFaxNum("509-989-3665");

        Contact contact1 = new Contact();
        contact1.setEmail("JJonesd@jacksoncarrier.com");
        contact1.setName("Jessica Jones");
        contact1.setPhoneNum("490-098-4433");
        contact1.setFaxNum("490-665-1010");

        Lane lane1 = new Lane();
        lane1.setFlatRate(3000);

        //Start in Chicago
        lane1.setStartLocationTitle("Chicago, Illinois, USA");
        lane1.setStartLat(41.8781);
        lane1.setStartLong(-87.6298);

        //End in Portland Oregon
        lane1.setEndLocationTitle("Portland, Oregon, USA");
        lane1.setEndLat(45.5231);
        lane1.setEndLong(-122.6765);
        lane1.setEquipmentType(Equipment.FLATBEAD);


        Lane lane2 = new Lane();
        lane2.setFlatRate(10000);

        //Start in Portland Oregon
        lane2.setStartLocationTitle("Portland, Oregon, USA");
        lane2.setStartLat(45.5231);
        lane2.setStartLong(-122.6765);

        //End in Chicago
        lane2.setEndLocationTitle("Chicago, Illinois, USA");
        lane2.setEndLat(41.8781);
        lane2.setEndLong(-87.6298);
        lane2.setEquipmentType(Equipment.REEFER);

        carrier.addLane(lane1);
        carrier.addLane(lane2);

        carrier.addContact(contact);
        carrier.addContact(contact1);

        return carrier;
    }

    public Carrier createThirdCarrierAndLane(){
        Carrier carrier = new Carrier();
        carrier.setTitle("Vanport Trucking");
        carrier.setNotes("Gets around!");
        carrier.setHomeCity("Boise, Idaho, USA");

        carrier.setHomeLat(43.618881);
        carrier.setHomeLong(-116.215019);

        Contact contact = new Contact();
        contact.setEmail("JThompson@banporttrucking.com");
        contact.setName("Justin Thompson");
        contact.setPhoneNum("733-446-2728");
        contact.setFaxNum("733-656-4112");

        Lane lane1 = new Lane();
        lane1.setFlatRate(3525);

        //Start in Boise
        lane1.setStartLocationTitle("Boise, Idaho, USA");

        lane1.setStartLat(43.618881);
        lane1.setStartLong(-116.215019);

        //End in Bend
        lane1.setEndLocationTitle("Bend, Oregon, USA");

        lane1.setEndLat(44.058174);
        lane1.setEndLong(-121.315308);
        lane1.setEquipmentType(Equipment.DRY_VAN);

        carrier.addLane(lane1);
        carrier.addContact(contact);

        return carrier;
    }

    public Carrier createTwinFallsCarrierAndLane(){
        Carrier carrier = new Carrier();
        carrier.setTitle("Great Western Trucking");
        carrier.setNotes("Gets around!");
        carrier.setHomeCity("Boise, Idaho, USA");

        carrier.setHomeLat(42.562786);
        carrier.setHomeLong(-114.460503);

        Lane lane1 = new Lane();
        lane1.setFlatRate(6600);

        //Start in Twin Falls
        lane1.setStartLocationTitle("Twin Falls, Idaho, USA");

        lane1.setStartLat(42.562786);
        lane1.setStartLong(-114.460503);

        //End in Los Angeles
        lane1.setEndLocationTitle("Los Angeles, California,USA");
        lane1.setEndLat(34.0522);
        lane1.setEndLong(-118.2437);
        lane1.setEquipmentType(Equipment.FLATBEAD);

        carrier.addLane(lane1);

        return carrier;
    }
}
