package com.lanetracker.lanesearcher.domain;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString(exclude = { "carrier" })
@Table(name = "lane", uniqueConstraints = { @UniqueConstraint(columnNames = { "start_lat", "start_long", "end_lat", "end_long", "carrier_id"  }) })
public class Lane {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "start_location_title", nullable = false)
    private String startLocationTitle;

    @Column(name = "start_lat", nullable = false)
    private double startLat;

    @Column(name = "start_long", nullable = false)
    private double startLong;

    @Column(name = "end_location_title", nullable = false)
    private String endLocationTitle;

    @Column(name = "end_lat", nullable = false)
    private double endLat;

    @Column(name = "end_long", nullable = false)
    private double endLong;

    @Column(name = "flat_rate", nullable = false)
    private double flatRate;

    @Column(name = "equipment_type")
    @Enumerated(EnumType.STRING)
    private Equipment equipmentType;

    @ManyToOne
    private Carrier carrier;
}
