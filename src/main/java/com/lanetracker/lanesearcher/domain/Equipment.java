package com.lanetracker.lanesearcher.domain;

public enum Equipment {
    FLATBEAD, DRY_VAN, REEFER
}
