package com.lanetracker.lanesearcher.domain;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ToString
@Table(name = "carrier", uniqueConstraints = { @UniqueConstraint(columnNames = { "title"  }) })
public class Carrier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false, unique = true)
    private String title;

    @Column(name = "notes")
    private String notes;

    @Column(name = "home_lat")
    private double homeLat;

    @Column(name = "home_long")
    private double homeLong;

    @Column(name = "home_city")
    private String homeCity;

    @Column(name = "mc_num")
    private String mcNum;

    @Column(name = "odot_num")
    private String odotNum;

    @Column(name = "deleted")
    private boolean deleted = false;

    @OneToMany(mappedBy = "carrier", cascade = CascadeType.ALL)
    private Set<Lane> lanes = new HashSet<>();

    @OneToMany(mappedBy = "carrier", cascade = CascadeType.ALL)
    private Set<Contact> contact = new HashSet<>();

    public void setLanes(List<Lane> lanes){
        if(lanes != null) {
            lanes.stream().forEach(lane -> addLane(lane));
        }
    }

    public void setContacts(List<Contact> contacts){
        if(contacts != null) {
            contacts.stream().forEach(contact -> addContact(contact));
        }
    }

    //Helper methods
    public void addLane(Lane lane){
        lane.setCarrier(this);
        this.lanes.add(lane);
    }

    public void addContact(Contact incomingContact){
        incomingContact.setCarrier(this);
        this.contact.add(incomingContact);
    }
}
