package com.lanetracker.lanesearcher.mediators;

import com.lanetracker.lanesearcher.domain.Carrier;
import com.lanetracker.lanesearcher.domain.Lane;
import com.lanetracker.lanesearcher.repository.CarrierRepository;
import com.lanetracker.lanesearcher.repository.LaneRepository;
import com.lanetracker.lanesearcher.rest.clients.GoogleRestClient;
import com.lanetracker.lanesearcher.services.dtos.crud.spreadsheetUpload.SpreadSheetUploadResultDTO;
import com.lanetracker.lanesearcher.services.dtos.crud.spreadsheetUpload.SpreadsheetRowDTO;
import com.lanetracker.lanesearcher.services.dtos.google.Location;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class CarrierMediator {

    private final CarrierRepository carrierRepository;
    private final LaneRepository laneRepository;
    private final GoogleRestClient googleRestClient;
    private final ModelMapper mapper;

    public CarrierMediator(CarrierRepository carrierRepository, LaneRepository laneRepository,
                           GoogleRestClient googleRestClient, ModelMapper mapper) {

        this.carrierRepository = carrierRepository;
        this.laneRepository = laneRepository;
        this.googleRestClient = googleRestClient;
        this.mapper = mapper;

    }

    public SpreadSheetUploadResultDTO processCarrierUpload(List<SpreadsheetRowDTO> spreadsheetUpload) {

        final SpreadSheetUploadResultDTO spreadSheetUploadResult = new SpreadSheetUploadResultDTO();
        final Map<String, Location> savedGeoSearchResults = new HashMap<>();
        final List<SpreadsheetRowDTO> successfulUploads = new ArrayList<>();
        final List<SpreadsheetRowDTO> failedUploads = new ArrayList<>();

        spreadsheetUpload.forEach(row -> {
            Carrier carrierResult = carrierRepository.findByTitleIgnoreCase(row.getCarrierDetails().getTitle());
            if(carrierResult == null) {
                carrierResult = carrierRepository.save(mapper.map(row.getCarrierDetails(), Carrier.class));
                log.info("Saved carrier: {}", carrierResult);
            };

            String startLocationTitle = row.getLaneDetails().getStartLocationTitle().trim();
            String endLocationTitle = row.getLaneDetails().getEndLocationTitle().trim();

            String startPlaceId = googleRestClient.getPlaceIdFromPlaceTitle(startLocationTitle);
            String endPlaceId = googleRestClient.getPlaceIdFromPlaceTitle(endLocationTitle);

            Location startCoords = getLocationFromPlaceId(startPlaceId, savedGeoSearchResults);
            Location endCoords = getLocationFromPlaceId(endPlaceId, savedGeoSearchResults);

            // If an error occurs, or a location is unable to be returned by google, add it to an error pool.
            // Error pool will be returned and shown to the user. User will need to either manually enter those
            // or fix the data and re-upload.

            // Assemble lane object
            Lane carrierLane = new Lane();
            carrierLane.setCarrier(carrierResult);
            carrierLane.setStartLocationTitle(startLocationTitle);
            carrierLane.setEndLocationTitle(endLocationTitle);
            carrierLane.setFlatRate(row.getLaneDetails().getFlatRate());
//            carrierLane.setEquipmentType();
            carrierLane.setStartLat(startCoords.getLat());
            carrierLane.setEndLat(startCoords.getLng());

            carrierLane.setEndLong(endCoords.getLng());
            carrierLane.setEndLat(endCoords.getLat());

            Lane savedLane = laneRepository.save(carrierLane);
            log.info("Saved Lane: {}", savedLane);

            // Add saved carrier/lane to successfulUpload
        });

        spreadSheetUploadResult.setSuccessfulUploads(successfulUploads);
        spreadSheetUploadResult.setFailedUploads(failedUploads);
        return spreadSheetUploadResult;
    };

    private Location getLocationFromPlaceId(String placeId, Map<String, Location> savedLocations) {
        if (savedLocations.containsKey(placeId)) {
            return savedLocations.get(placeId);
        } else {
            Location googleResults = googleRestClient.getLocationFromPlaceId(null, placeId);
            savedLocations.put(placeId, googleResults);
            return googleResults;
        }
    }
}
