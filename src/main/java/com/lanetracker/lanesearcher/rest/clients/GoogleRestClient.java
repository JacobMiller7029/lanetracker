package com.lanetracker.lanesearcher.rest.clients;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lanetracker.lanesearcher.rest.exceptions.NoLocationResultException;
import com.lanetracker.lanesearcher.services.dtos.google.GeocodeResult;
import com.lanetracker.lanesearcher.services.dtos.google.Location;
import jdk.nashorn.internal.ir.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Slf4j
@Component
public class GoogleRestClient {

    private static final String PLACES_URL = "https://maps.googleapis.com/maps/api/place/autocomplete/json?components=country:us&types=(cities)&input=";
    private static final String GEOCODED_URL = "https://maps.googleapis.com/maps/api/geocode/json?place_id=";

    private final RestTemplate restClient;
    private final ObjectMapper mapper;
    private final String AND_KEY;


    @Autowired
    public GoogleRestClient(RestTemplate restTemplate, @Value("${google.api.key}") String apiKey, ObjectMapper objMapper) {
        this.restClient = restTemplate;
        this.AND_KEY = "&key=" + apiKey;
        this.mapper = objMapper;
    }

    public Location getLocationFromPlaceId(Pageable pageable, String placeId) {
        final String url = GEOCODED_URL + placeId + AND_KEY;
        log.debug("Attempting geocoding from placeid {} with url: {}", placeId, url);

        HttpHeaders headers = new HttpHeaders();

        ResponseEntity<GeocodeResult> entityDto = restClient.exchange(
                url, HttpMethod.GET, new HttpEntity<>("", headers),
                GeocodeResult.class);

        return getLocationFromGeocodeResults(entityDto.getBody());
    }

    public JsonNode getPlacesAutocomplete(String queryString) {
        String requestUrl = PLACES_URL + queryString + AND_KEY;

        ResponseEntity<JsonNode> response = restClient
                .exchange(requestUrl, HttpMethod.GET, new HttpEntity<>("", new HttpHeaders()), JsonNode.class);

        return response.getBody();
    }

    public Location getLocationFromGeocodeResults(GeocodeResult result) {
        if(result.getResults().size() == 0){
            throw new NoLocationResultException("No result found: " + result.toString());
        }
        return result.getResults().get(0).getGeometry().getLocation();
    }

    // TODO: What if it can't find a place id?
    // TODO: What if there are multiple places returned?
    public String getPlaceIdFromPlaceTitle(String placeTitle) {
        log.info("Searching for placeTitle: {}", placeTitle);
        JsonNode placesResult = getPlacesAutocomplete(placeTitle);
        String placeId = placesResult.get("predictions").get(0).get("place_id").asText();
        log.info("placeId: {}", placeId);
        return placeId;
    }

}
