package com.lanetracker.lanesearcher.rest.controllers.query_params;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class LaneQueryParams {
    String start;
    String end;
    Long radius;
    boolean bidirectional;
}
