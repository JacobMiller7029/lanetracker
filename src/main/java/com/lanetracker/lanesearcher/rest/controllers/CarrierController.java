package com.lanetracker.lanesearcher.rest.controllers;

import com.lanetracker.lanesearcher.rest.controllers.generic.AbstractCrudController;
import com.lanetracker.lanesearcher.rest.controllers.query_params.CarrierQueryParams;
import com.lanetracker.lanesearcher.services.dtos.crud.CarrierDTO;
import com.lanetracker.lanesearcher.services.interfaces.CarrierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
@CrossOrigin
@RequestMapping("/carrier")
public class CarrierController extends AbstractCrudController<CarrierDTO, CarrierQueryParams> {
    public CarrierController(CarrierService carrierService) {
        super(CarrierDTO.class.getSimpleName(), carrierService);
    }
}
