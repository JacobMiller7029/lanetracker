package com.lanetracker.lanesearcher.rest.controllers;

import com.lanetracker.lanesearcher.mediators.CarrierMediator;
import com.lanetracker.lanesearcher.services.dtos.crud.spreadsheetUpload.SpreadSheetUploadResultDTO;
import com.lanetracker.lanesearcher.services.dtos.crud.spreadsheetUpload.SpreadsheetRowDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@CrossOrigin
@RestController("/bulkupload")
public class BulkUploadController {

    private final CarrierMediator carrierMediator;

    public BulkUploadController(CarrierMediator carrierMediator) {
        this.carrierMediator = carrierMediator;
    }

    @ResponseBody
    @PostMapping
    public ResponseEntity saveNewEntity(@RequestBody List<SpreadsheetRowDTO> entityToBeSaved) {
        log.info("POST: SpreadsheetRowDTO; received: {}", entityToBeSaved);
        SpreadSheetUploadResultDTO savedRows = carrierMediator.processCarrierUpload(entityToBeSaved);
        log.info("Uploaded entries result: {}", savedRows);
        return new ResponseEntity<>(savedRows, HttpStatus.CREATED);
    }
}
