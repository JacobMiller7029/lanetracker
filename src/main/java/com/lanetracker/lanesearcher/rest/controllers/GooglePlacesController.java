package com.lanetracker.lanesearcher.rest.controllers;


import com.fasterxml.jackson.databind.JsonNode;
import com.lanetracker.lanesearcher.rest.clients.GoogleRestClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller
@CrossOrigin
@RequestMapping("/google")
public class GooglePlacesController {
    private static final String GOOLGE_PLACES = "https://maps.googleapis.com/maps/api/place/autocomplete/json?components=country:us&types=(cities)";

    private final GoogleRestClient restClient;

    @Autowired
    public GooglePlacesController(GoogleRestClient googleRestClient) {
        this.restClient = googleRestClient;
    }

    /**
     * This controller is needed during development
     * to bypass CORS and COBS issues on Chrome.
     *
     * There is potential this will be used in production.
     * If its used in production, more work will be done
     * to secure the key.
     *
     * @param locationInput
     * @return
     */
    @ResponseBody
    @GetMapping
    public ResponseEntity<JsonNode> proxyGooglePlacesRequest(
            @RequestParam(value = "input") String locationInput) {

        //TODO: Create a more intelligent response if needed.
        if( locationInput.length() >= 3 ){
            JsonNode response = restClient.getPlacesAutocomplete(locationInput);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
