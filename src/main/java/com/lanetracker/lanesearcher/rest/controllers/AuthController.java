package com.lanetracker.lanesearcher.rest.controllers;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.lanetracker.lanesearcher.security.jwt.TokenProvider;
import com.lanetracker.lanesearcher.services.dtos.LoginSuccessResponse;
import com.lanetracker.lanesearcher.services.dtos.UserDto;
import com.lanetracker.lanesearcher.services.interfaces.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@CrossOrigin
@RestController
public class AuthController {

    private final TokenProvider tokenProvider;
    private final AuthenticationManager authenticationManager;

    @Autowired
    public AuthController(TokenProvider tokenProvider,
                          AuthenticationManager authenticationManager) {
        this.tokenProvider = tokenProvider;
        this.authenticationManager = authenticationManager;
    }

    @GetMapping("/authenticate")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void authenticate() {
        // Don't have to do anything here
        // this is just a secure endpoint and the JWTFilter
        // validates the token
        // this service is called at startup of the app to check
        // if the jwt token is still valid
    }

    @PostMapping("/login")
    public ResponseEntity authorize(@Valid @RequestBody UserDto loginUser,
                                    HttpServletResponse response) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                loginUser.getUsername(), loginUser.getPassword());

        try {
            this.authenticationManager.authenticate(authenticationToken);

            LoginSuccessResponse loginResponse = new LoginSuccessResponse(this.tokenProvider
                    .createToken(loginUser.getUsername()), this.tokenProvider.generateTokenExpirationDate());

            return new ResponseEntity<>(loginResponse, HttpStatus.OK);
        }
        catch (AuthenticationException e) {
            log.info("Controller Security exception {}", e.getMessage());
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }
    }
}
