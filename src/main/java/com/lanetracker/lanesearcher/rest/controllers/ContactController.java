package com.lanetracker.lanesearcher.rest.controllers;

import com.lanetracker.lanesearcher.rest.controllers.generic.AbstractCrudController;
import com.lanetracker.lanesearcher.rest.controllers.query_params.ContactQueryParams;
import com.lanetracker.lanesearcher.services.dtos.crud.ContactDto;
import com.lanetracker.lanesearcher.services.interfaces.ContactService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;


@Slf4j
@Controller
@CrossOrigin
@RequestMapping("/contacts")
public class ContactController extends AbstractCrudController<ContactDto, ContactQueryParams> {
    @Autowired
    public ContactController(ContactService contactService) {
        super(ContactDto.class.getSimpleName(), contactService);
    }
}
