package com.lanetracker.lanesearcher.rest.controllers;

import com.lanetracker.lanesearcher.rest.controllers.generic.AbstractCrudController;
import com.lanetracker.lanesearcher.rest.controllers.query_params.LaneQueryParams;
import com.lanetracker.lanesearcher.services.dtos.crud.LaneDTO;
import com.lanetracker.lanesearcher.services.interfaces.LaneService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
@CrossOrigin
@RequestMapping("/lanes")
public class LaneController extends AbstractCrudController<LaneDTO, LaneQueryParams> {
    public LaneController(LaneService laneService) {
        super(LaneDTO.class.getSimpleName(), laneService);
    }
}
