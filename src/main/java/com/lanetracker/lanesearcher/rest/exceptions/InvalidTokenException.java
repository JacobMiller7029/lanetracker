package com.lanetracker.lanesearcher.rest.exceptions;

import lombok.Getter;

@Getter
public class InvalidTokenException extends RuntimeException {

    private String message;

    public InvalidTokenException(String message) {
        super(message);
    }
}
