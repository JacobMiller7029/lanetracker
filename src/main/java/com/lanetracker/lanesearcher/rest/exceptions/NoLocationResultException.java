package com.lanetracker.lanesearcher.rest.exceptions;

public class NoLocationResultException extends RuntimeException  {
    public NoLocationResultException(String message) { super(message); }
}
