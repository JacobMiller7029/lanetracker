package com.lanetracker.lanesearcher.services.exceptions;

public class NoLaneLocationException extends RuntimeException {
    public NoLaneLocationException(String message) {
        super(message);
    }
}
