package com.lanetracker.lanesearcher.services.exceptions;

public class NoParentCarrier extends RuntimeException {
    public NoParentCarrier(String message) {
        super(message);
    }
}
