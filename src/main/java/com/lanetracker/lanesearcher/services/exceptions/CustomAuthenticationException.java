package com.lanetracker.lanesearcher.services.exceptions;

import javax.naming.AuthenticationException;

public class CustomAuthenticationException extends AuthenticationException {
    public CustomAuthenticationException(String explanation) {
        super(explanation);
    }
}
