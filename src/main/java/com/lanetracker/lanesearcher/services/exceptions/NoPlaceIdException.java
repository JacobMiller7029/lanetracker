package com.lanetracker.lanesearcher.services.exceptions;

public class NoPlaceIdException extends RuntimeException {
    public NoPlaceIdException(String message) {
        super(message);
    }
}
