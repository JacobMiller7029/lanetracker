package com.lanetracker.lanesearcher.services;

import com.lanetracker.lanesearcher.domain.Lane;
import com.lanetracker.lanesearcher.repository.LaneRepository;
import com.lanetracker.lanesearcher.rest.clients.GoogleRestClient;
import com.lanetracker.lanesearcher.rest.controllers.query_params.LaneQueryParams;
import com.lanetracker.lanesearcher.services.dtos.crud.LaneDTO;
import com.lanetracker.lanesearcher.services.dtos.google.Location;
import com.lanetracker.lanesearcher.services.exceptions.NoLaneLocationException;
import com.lanetracker.lanesearcher.services.exceptions.NoParentCarrier;
import com.lanetracker.lanesearcher.services.interfaces.LaneService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class LaneServiceImpl implements LaneService {

    private static final int END_LOCATION = 1;
    private static final int START_LOCATION = 0;

    private final LaneRepository laneRepository;
    private final GoogleRestClient googleRestClient;
    private final ModelMapper mapper;

    public LaneServiceImpl(LaneRepository laneRepository, GoogleRestClient googleRestClient, ModelMapper mapper) {
        this.laneRepository = laneRepository;
        this.googleRestClient = googleRestClient;
        this.mapper = mapper;
    }

    @Override
    public List<LaneDTO> getListOfEntities(LaneQueryParams queryParams) {
        final Location startCoords = googleRestClient.getLocationFromPlaceId(null, queryParams.getStart());
        final Location endCoords = googleRestClient.getLocationFromPlaceId(null, queryParams.getEnd());

        Iterable<Lane> results = laneRepository.searchForLanes(startCoords.getLat(), startCoords.getLng(),
                endCoords.getLat(), endCoords.getLng(), queryParams.getRadius());

        if(queryParams.isBidirectional()) {
            Iterable<Lane> bidirectionalResults = laneRepository.searchForLanes(endCoords.getLat(), endCoords.getLng(),
                    startCoords.getLat(), startCoords.getLng(), queryParams.getRadius());

            results = CollectionUtils.union(results, bidirectionalResults);
        }

        // TODO: Should add a delete field to the lane object and delete the lane when the parent gets deleted.
        // This just filters out the lanes that belong to deleted carriers. Not the greatest approach.
        return StreamSupport.stream(results.spliterator(), false)
                .filter(result -> !result.getCarrier().isDeleted())
                .map(result -> mapper.map(result, LaneDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public LaneDTO getEntityById(Long id) {
        return null;
    }

    @Override
    public List<LaneDTO> saveEntities(List<LaneDTO> lanes) {

        final List<Lane> lanesToBeSaved = new ArrayList<>();
        final List<LaneDTO> lanesToReturn = new ArrayList<>();

        lanes.forEach(lane -> {
            Lane laneToBeSaved = mapper.map(lane, Lane.class);

            if (lane.getCarrier() == null || lane.getCarrier().getId() == null) {
                throw new NoParentCarrier("Attempted to save lane without attached carrier.");
            }

            Location[] locations = placeIdToLocationList(lane);

            if (locations != null && locations.length == 2) {
                laneToBeSaved.setStartLat(locations[START_LOCATION].getLat());
                laneToBeSaved.setStartLong(locations[START_LOCATION].getLng());

                laneToBeSaved.setEndLat(locations[END_LOCATION].getLat());
                laneToBeSaved.setEndLong(locations[END_LOCATION].getLng());
            } else {
                throw new NoLaneLocationException("locations are null");
            }

            lanesToBeSaved.add(laneToBeSaved);
        });

        Iterable<Lane> savedLanes = laneRepository.saveAll(lanesToBeSaved);

        savedLanes.iterator().forEachRemaining(lane -> lanesToReturn.add(mapper.map(lane, LaneDTO.class)));

        return lanesToReturn;
    }

    @Override
    public LaneDTO updateEntity(LaneDTO lane) {
        Lane laneToBeSaved = mapper.map(lane, Lane.class);

        if (lane.getCarrier() == null || lane.getCarrier().getId() == null) {
            throw new NoParentCarrier("Attempted to save lane without attached carrier.");
        }

        if(lane.getStartPlaceId() != null && !lane.getStartPlaceId().isEmpty()){
            Location location = googleRestClient.getLocationFromPlaceId(null, lane.getStartPlaceId());
            laneToBeSaved.setStartLat(location.getLat());
            laneToBeSaved.setStartLong(location.getLng());
        }

        if(lane.getEndPlaceId() != null && !lane.getEndPlaceId().isEmpty()){
            Location location = googleRestClient.getLocationFromPlaceId(null, lane.getEndPlaceId());
            laneToBeSaved.setEndLat(location.getLat());
            laneToBeSaved.setEndLong(location.getLng());
        }

        return mapper.map(laneRepository.save(laneToBeSaved), LaneDTO.class);
    }

    @Override
    public void deleteEntityById(Long id) {
        laneRepository.deleteById(id);
    }

    private Location[] placeIdToLocationList(LaneDTO input) {
        Location[] locationList = new Location[2];

        if (!input.getStartPlaceId().isEmpty() && !input.getEndPlaceId().isEmpty()) {
            Location startLocation = googleRestClient.getLocationFromPlaceId(null, input.getStartPlaceId());
            Location endLocation = googleRestClient.getLocationFromPlaceId(null, input.getEndPlaceId());

            locationList[START_LOCATION] = startLocation;
            locationList[END_LOCATION] = endLocation;

            return locationList;
        }

        return null;
    }
}
