package com.lanetracker.lanesearcher.services;

import com.lanetracker.lanesearcher.domain.Contact;
import com.lanetracker.lanesearcher.repository.ContactRepository;
import com.lanetracker.lanesearcher.rest.controllers.query_params.ContactQueryParams;
import com.lanetracker.lanesearcher.services.dtos.crud.ContactDto;
import com.lanetracker.lanesearcher.services.exceptions.NoParentCarrier;
import com.lanetracker.lanesearcher.services.interfaces.ContactService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ContactServiceImpl implements ContactService {

    private final ModelMapper mapper;
    private final ContactRepository repository;

    @Autowired
    public ContactServiceImpl(ModelMapper mapper, ContactRepository repository) {
        this.mapper = mapper;
        this.repository = repository;
    }

    @Override
    public List<ContactDto> getListOfEntities(ContactQueryParams queryParams) {
        return null;
    }

    @Override
    public ContactDto getEntityById(Long id) {
        log.debug("Retrieving entity with ID: ", id);
        return mapper.map(repository.findById(id), ContactDto.class);
    }

    @Override
    public List<ContactDto> saveEntities(List<ContactDto> contactDtos) {

        final List<Contact> contactsToSave = new ArrayList<>();

        contactDtos.forEach(contactDto -> {

            if (contactDto.getCarrier() == null || contactDto.getCarrier().getId() == null) {
                throw new NoParentCarrier("Cannot save Contact without Carrier");
            }

            contactDto.setName(nameToUppercase(contactDto.getName()));

            contactsToSave.add(mapper.map(contactDto, Contact.class));
        });

        final List<ContactDto> contactsToReturn = new ArrayList<>();
        Iterable<Contact> savedContacts = repository.saveAll(contactsToSave);

        savedContacts.iterator()
                .forEachRemaining(contact -> contactsToReturn.add(mapper.map(contact, ContactDto.class)));

        return contactsToReturn;
    }

    @Override
    public ContactDto updateEntity(ContactDto entityToUpdate) {
        log.debug("Updating Contact: {}", entityToUpdate);

        if (entityToUpdate.getCarrier() == null || entityToUpdate.getCarrier().getId() == null) {
            throw new NoParentCarrier("Attempted to save lane, can not save without carrier attached.");
        }

        entityToUpdate.setName(nameToUppercase(entityToUpdate.getName()));
        final Contact contactToBeSave = mapper.map(entityToUpdate, Contact.class);
        return mapper.map(repository.save(contactToBeSave), ContactDto.class);
    }

    @Override
    public void deleteEntityById(Long id) {
        log.debug("DELETE - Contact with ID: {}", id);
        repository.deleteById(id);
    }

    private String nameToUppercase(String name) {
        // split into words
        String[] words = name.split(" ");

        // capitalize each word
        for (int i = 0; i < words.length; i++) {
            words[i] = words[i].substring(0, 1).toUpperCase() + words[i].substring(1).toLowerCase();
        }

        // rejoin back into a sentence
        return String.join(" ", words);
    }
}
