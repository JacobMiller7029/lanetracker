package com.lanetracker.lanesearcher.services.dtos.crud;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = { "carrier" })
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LaneDTO {

    private Long id;
    private String startLocationTitle;
    private String startPlaceId;
    private double startLat;
    private double startLong;

    private String endLocationTitle;
    private String endPlaceId;
    private double endLat;
    private double endLong;

    private double flatRate;

    private CarrierSearchResultDTO carrier;
}
