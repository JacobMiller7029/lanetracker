package com.lanetracker.lanesearcher.services.dtos.crud.spreadsheetUpload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SpreadsheetDTO {

    private List<SpreadsheetRowDTO> spreadSheetRows;

}
