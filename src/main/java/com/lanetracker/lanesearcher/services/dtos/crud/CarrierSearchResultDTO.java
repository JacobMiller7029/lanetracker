package com.lanetracker.lanesearcher.services.dtos.crud;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CarrierSearchResultDTO {
    private Long id;
    private String title;
}
