package com.lanetracker.lanesearcher.services.dtos.google;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class GeocodeResult {
    private List<GeocodeResponse> results = new ArrayList();
    private String status;
}
