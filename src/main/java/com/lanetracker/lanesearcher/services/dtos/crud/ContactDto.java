package com.lanetracker.lanesearcher.services.dtos.crud;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = { "carrier" })
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContactDto {

    private Long id;
    private String name;
    private String phoneNum;
    private String email;
    private String faxNum;

    @JsonBackReference
    private CarrierDTO carrier;

}
