package com.lanetracker.lanesearcher.services.dtos.crud;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = { "lanes" })
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CarrierDTO {

    private Long id;
    private String title;
    private String notes;
    private double homeLat;
    private double homeLong;
    private String placeId;
    private String homeCity;
    private String odotNum;
    private String mcNum;

    @JsonManagedReference
    private List<LaneDTO> lanes = new ArrayList();

    @JsonManagedReference
    private List<ContactDto> contact = new ArrayList();;

    @Override
    public String toString() {
        return "CarrierDTO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", notes='" + notes + '\'' +
                ", homeLat=" + homeLat +
                ", homeLong=" + homeLong +
                ", homeCity='" + homeCity + '\'' +
                ", lanes=" + lanes +
                ", contact=" + contact +
                '}';
    }
}
