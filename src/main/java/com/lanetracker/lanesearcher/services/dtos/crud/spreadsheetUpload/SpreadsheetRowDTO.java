package com.lanetracker.lanesearcher.services.dtos.crud.spreadsheetUpload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.lanetracker.lanesearcher.services.dtos.crud.CarrierSearchResultDTO;
import com.lanetracker.lanesearcher.services.dtos.crud.LaneDTO;
import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class SpreadsheetRowDTO {

    private CarrierSearchResultDTO carrierDetails;
    private LaneDTO laneDetails;

}
