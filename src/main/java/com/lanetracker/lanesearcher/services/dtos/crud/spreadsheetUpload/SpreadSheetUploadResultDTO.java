package com.lanetracker.lanesearcher.services.dtos.crud.spreadsheetUpload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class SpreadSheetUploadResultDTO {

    private List<SpreadsheetRowDTO> successfulUploads;
    private List<SpreadsheetRowDTO> failedUploads;

}
