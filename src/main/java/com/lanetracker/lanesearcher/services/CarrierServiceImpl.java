package com.lanetracker.lanesearcher.services;

import com.lanetracker.lanesearcher.configuration.Constants;
import com.lanetracker.lanesearcher.domain.Carrier;
import com.lanetracker.lanesearcher.repository.CarrierRepository;
import com.lanetracker.lanesearcher.rest.clients.GoogleRestClient;
import com.lanetracker.lanesearcher.rest.controllers.query_params.CarrierQueryParams;
import com.lanetracker.lanesearcher.services.dtos.crud.CarrierDTO;
import com.lanetracker.lanesearcher.services.dtos.google.Location;
import com.lanetracker.lanesearcher.services.interfaces.CarrierService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class CarrierServiceImpl implements CarrierService {

    private final CarrierRepository carrierRepository;
    private final GoogleRestClient restClient;
    private final ModelMapper modelMapper;

    public CarrierServiceImpl(CarrierRepository carrierRepository,
                              ModelMapper modelMapper,
                              GoogleRestClient restClient)
    {
        this.carrierRepository = carrierRepository;
        this.restClient = restClient;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<CarrierDTO> getListOfEntities(CarrierQueryParams queryParams) {
        log.info("Incoming search request - Type: {}: Query: {}", queryParams.getType(), queryParams.getQuery());

        String type = queryParams.getType();
        String query = queryParams.getQuery();

        Iterable<Carrier> carrierSearchResult = null;

        if (type.toLowerCase().equals(Constants.SEARCH_TYPE_MC)) {
            carrierSearchResult = carrierRepository.findByMcNumContainingIgnoreCaseAndDeletedIsFalse(query);
        } else if (type.toLowerCase().equals(Constants.SEARCH_TYPE_ODOT)) {
            carrierSearchResult = carrierRepository.findByOdotNumContainingIgnoreCaseAndDeletedIsFalse(query);
        } else if (type.toLowerCase().equals(Constants.SEARCH_TYPE_NAME)) {
            carrierSearchResult = carrierRepository.findByTitleContainingIgnoreCaseAndDeletedIsFalse(query);
        }

        return StreamSupport.stream(carrierSearchResult.spliterator(), false)
                .map(result -> modelMapper.map(result, CarrierDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<CarrierDTO> saveEntities(List<CarrierDTO> carrierDTOs) {
        log.debug("CarrierDto to be saved: {}", carrierDTOs.toString());

        carrierDTOs.forEach(carrierDTO -> {
                        if (carrierDTO.getHomeLat() == 0 && carrierDTO.getHomeLong() == 0 && !carrierDTO.getPlaceId().isEmpty()) {
                            Location result = restClient.getLocationFromPlaceId(null, carrierDTO.getPlaceId());
                            carrierDTO.setHomeLat(result.getLat());
                            carrierDTO.setHomeLong(result.getLng());
                        }
                 }
        );

        final List<Carrier> carriersToBeSaved = new ArrayList<>();
        carrierDTOs.forEach(carrierDTO -> carriersToBeSaved.add(modelMapper.map(carrierDTO, Carrier.class)));
        final Iterable<Carrier> savedCarriers = carrierRepository.saveAll(carriersToBeSaved);
        final List<CarrierDTO> returnedCarriers = new ArrayList<>();

        savedCarriers.iterator()
                .forEachRemaining(carrier -> returnedCarriers.add(modelMapper.map(carrier, CarrierDTO.class)));

        return returnedCarriers;
    }

    @Override
    public CarrierDTO getEntityById(Long id) {
        log.debug("Retrieving carrier with ID {}", id);
        Carrier carrier = carrierRepository.findById(id).get();
        return modelMapper.map(carrier, CarrierDTO.class);
    }

    @Override
    public CarrierDTO updateEntity(CarrierDTO carrierToUpdate) {
        log.info("updating Carrier with ID: {}", carrierToUpdate.getId());

        if(!carrierToUpdate.getPlaceId().isEmpty())
        {
            Location result = restClient.getLocationFromPlaceId(null, carrierToUpdate.getPlaceId());
            carrierToUpdate.setHomeLat(result.getLat());
            carrierToUpdate.setHomeLong(result.getLng());
        }

        final Carrier carrierToBeSaved = modelMapper.map(carrierToUpdate, Carrier.class);
        Carrier savedCarrier = carrierRepository.save(carrierToBeSaved);
        log.info("Carrier after being updated: {}", savedCarrier.toString());
        return modelMapper.map(savedCarrier, CarrierDTO.class);
    }

    @Override
    public void deleteEntityById(Long id) {
        log.debug("Carrier with ID {} being deleted...", id);
        Carrier carrier = carrierRepository.findById(id).get();
        carrier.setDeleted(true);
        carrierRepository.save(carrier);
        log.debug("Carrier with ID {} successfully soft deleted.", id);
    }
}
