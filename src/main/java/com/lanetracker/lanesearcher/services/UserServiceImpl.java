package com.lanetracker.lanesearcher.services;

import com.lanetracker.lanesearcher.domain.User;
import com.lanetracker.lanesearcher.services.interfaces.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;

@Slf4j
@Component
public class UserServiceImpl implements UserService {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User findByUsername(String username) {
        final User SYSTEM_USER = new User("admin", passwordEncoder.encode("admin"), new ArrayList<>());

        if(SYSTEM_USER.getUsername().equals(username)) {
            return SYSTEM_USER;
        }

        throw new UsernameNotFoundException("Username " + username + " Does note exist.");
    }

    @Override
    public User addNewUser(User newUser) {
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findByUsername(username);

        return User.withUsername(username)
                .password(user.getPassword()).authorities(Collections.emptyList())
                .accountExpired(false).accountLocked(false).credentialsExpired(false)
                .disabled(false).build();
    }
}
