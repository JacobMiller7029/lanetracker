package com.lanetracker.lanesearcher.services.interfaces;


import com.lanetracker.lanesearcher.domain.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    User findByUsername(String username);
    User addNewUser(User newUser);
}
