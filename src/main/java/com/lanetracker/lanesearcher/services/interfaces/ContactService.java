package com.lanetracker.lanesearcher.services.interfaces;

import com.lanetracker.lanesearcher.rest.controllers.query_params.ContactQueryParams;
import com.lanetracker.lanesearcher.services.dtos.crud.ContactDto;

public interface ContactService extends CrudService<ContactDto, ContactQueryParams> {
}
