package com.lanetracker.lanesearcher.services.interfaces;

import com.lanetracker.lanesearcher.rest.controllers.query_params.LaneQueryParams;
import com.lanetracker.lanesearcher.services.dtos.crud.LaneDTO;

public interface LaneService extends CrudService<LaneDTO, LaneQueryParams> {
}
