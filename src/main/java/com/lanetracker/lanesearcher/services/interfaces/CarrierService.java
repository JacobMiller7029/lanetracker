package com.lanetracker.lanesearcher.services.interfaces;

import com.lanetracker.lanesearcher.rest.controllers.query_params.CarrierQueryParams;
import com.lanetracker.lanesearcher.services.dtos.crud.CarrierDTO;

public interface CarrierService extends CrudService<CarrierDTO, CarrierQueryParams> {
}
