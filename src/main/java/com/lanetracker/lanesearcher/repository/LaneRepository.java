package com.lanetracker.lanesearcher.repository;

import com.lanetracker.lanesearcher.domain.Lane;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface LaneRepository extends CrudRepository<Lane, Long> {

    @Query(value = "SELECT * FROM lane \n" +
            "WHERE ((point(?4, ?3) <@> point(end_long, end_lat)) < ?5)\n" +
            "AND ((point(?2, ?1) <@> point(start_long, start_lat)) < ?5)\n", nativeQuery=true)
    Iterable<Lane> searchForLanes(double startLat, double startLong, double endLat, double endLong, double searchRadius);

}


//@Query(value = "SELECT * FROM lane \n" +
//        "WHERE ((point(122.6716, 45.6318) <@> point(end_long, end_lat)) < 15)\n" +
//        "AND ((point(118.1332, 33.9401) <@> point(start_long, start_lat)) < 15)\n", nativeQuery=true)
