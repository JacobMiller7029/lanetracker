package com.lanetracker.lanesearcher.repository;

import com.lanetracker.lanesearcher.domain.Lane;
import org.springframework.data.repository.CrudRepository;

public interface SearchDoa extends CrudRepository<Lane, Long> {



}
