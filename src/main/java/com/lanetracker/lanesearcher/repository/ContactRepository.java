package com.lanetracker.lanesearcher.repository;

import com.lanetracker.lanesearcher.domain.Contact;
import org.springframework.data.repository.CrudRepository;

public interface ContactRepository extends CrudRepository<Contact, Long> {
}
