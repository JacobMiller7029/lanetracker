package com.lanetracker.lanesearcher.repository;

import com.lanetracker.lanesearcher.domain.Carrier;
import org.springframework.data.repository.CrudRepository;

public interface CarrierRepository extends CrudRepository<Carrier, Long> {

    Iterable<Carrier> findByTitleContainingIgnoreCaseAndDeletedIsFalse(String name);

    Iterable<Carrier> findByOdotNumContainingIgnoreCaseAndDeletedIsFalse(String odotNum);

    Iterable<Carrier> findByMcNumContainingIgnoreCaseAndDeletedIsFalse(String mcNum);

    Carrier findByTitleIgnoreCase(String title);
}
