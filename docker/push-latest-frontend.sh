#!/usr/bin/env bash

echo "[SCRIPT] - Building docker image"
docker build -f ./docker/react/Dockerfile . -t jacobmiller7029/lanetracker-frontend:latest

if [ $? -eq 0 ]; then
    echo "[SCRIPT] - Pushing docker image to docker repository"
    docker push jacobmiller7029/lanetracker-frontend:latest
else
    echo "[SCRIPT] - Docker build failed. Skipping push."
    exit 1;
fi
