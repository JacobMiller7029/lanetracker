#! /bin/bash
# Script for starting application within docker with custom variables.
# For local development, consider using the start-local.sh script

java -Djava.security.egd=file:/dev/./urandom -jar ./app.jar --spring.profiles.active=$1 --spring.datasource.username=$2 --spring.datasource.password=$3 --google.api.key=$4 --lanetracker.secret=$5
