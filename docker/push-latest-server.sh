#!/bin/bash
# Script for building and pushing latest code to docker repository.

echo "[SCRIPT] - Cleaning project"
sudo mvn clean -o

echo "[SCRIPT] - Building jar, skipping tests..."
sudo mvn install -DskipTests

echo "[SCRIPT] - Building docker image"
docker build -f ./docker/Dockerfile . -t jacobmiller7029/lanetracker:latest

echo "[SCRIPT] - Pushing docker image to docker repository"
docker push jacobmiller7029/lanetracker:latest
