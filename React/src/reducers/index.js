import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import { carrierSearch, laneSearch } from './searchResultsReducer';
import { setSearchType, setLaneSearchInProgress } from './searchReducer';
import { selectedCarrier, displaySelectedCarrier } from './selectedCarrierReducer';
import { contactModalStatus, laneModalStatus, carrierModalStatus } from './modalsReducer';
import { authToken, loginErrorModal, forcedLogoutModal } from './authReducer';

export default combineReducers({
    authToken,
    carrierSearchResults: carrierSearch,
    displayCarrierDetails: displaySelectedCarrier,
    displayLoginErrorModal: loginErrorModal,
    displayForcedLogoutModal: forcedLogoutModal,
    laneSearchResults: laneSearch,
    laneSearchInProgress: setLaneSearchInProgress,
    newContactModal: contactModalStatus,
    newCarrierModal: carrierModalStatus,
    newLaneModal: laneModalStatus,
    routing: routerReducer,
    searchType: setSearchType,
    selectedCarrier: selectedCarrier,
});
