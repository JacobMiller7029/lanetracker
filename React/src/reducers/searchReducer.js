import actionsConstants from '../res/strings/action_constants';
import searchConstants from '../res/strings/search_constants';

export const setSearchType = (state = searchConstants.SEARCH_LANE, action) => {
    switch (action.type) {
        case actionsConstants.SEARCH_TYPE_LANE:
            return searchConstants.SEARCH_LANE
        case actionsConstants.SEARCH_TYPE_CARRIER:
            return searchConstants.SEARCH_CARRIER
        default:
            return state
    }
}

export const setLaneSearchInProgress = (state = false, action) => {
    switch (action.type) {
        case actionsConstants.SEARCH_IN_PROGRESS:
            return state;
        default:
            return false
    }
}

