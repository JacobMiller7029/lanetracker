import constants from '../res/strings/action_constants';

export const contactModalStatus = (state = false, action) => {
    switch (action.type) {
        case constants.SHOW_CONTACT_MODAL:
            return action.payload;
        case constants.HIDE_CONTACT_MODAL:
            return action.payload;
        default:
            return state
    }
}

export const laneModalStatus = (state = false, action) => {
    switch (action.type) {
        case constants.SHOW_LANE_MODAL:
            return action.payload;
        case constants.HIDE_LANE_MODAL:
            return action.payload;
        default:
            return state
    }
}

export const carrierModalStatus = (state = false, action) => {
    switch (action.type) {
        case constants.SHOW_NEW_CARRIER_MODAL:
            return action.payload;
        case constants.HIDE_NEW_CARRIER_MODAL:
            return action.payload;
        default:
            return state;
    }
}
