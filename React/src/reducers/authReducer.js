import constants from '../res/strings/action_constants';
import { loadToken, loadTokenExperationDate } from '../actions/auth-action-index';
import statusEnums from '../res/enums/AuthorizationStatusEnums';

export const authToken = (state = { status: loadToken() ? statusEnums.HAS_TOKEN : statusEnums.INITIAL_STATE, expires: loadTokenExperationDate(), token: loadToken() }, action) => {
    switch (action.type) {
        case constants.SET_AUTH_TOKEN:
            localStorage.setItem(constants.SET_AUTH_TOKEN, JSON.stringify(action.payload));
            return action.payload;
        case constants.REMOVE_AUTH_TOKEN:
            localStorage.removeItem(constants.SET_AUTH_TOKEN);
            return { status: action.payload, expires: null, token: null };
        default:
            return state
    }
}

export const forcedLogoutModal = (state = false, action) => {
    switch (action.type) {
        case constants.SHOW_FOCED_LOGOUT_MODAL:
            return true;
        case constants.HIDE_FORCED_LOGOUT_MODAL:
            return false;
        default:
            return state;
    }
}

export const loginErrorModal = (state = false, action) => {
    switch (action.type) {
        case constants.SHOW_LOGIN_ERROR_MODAL:
            return true;
        case constants.HIDE_LOGIN_ERROR_MODAL:
            return false;
        default:
            return state;
    }
}