import * as _ from 'lodash';
import constants from '../res/strings/action_constants';

export const selectedCarrier = (state = {}, action) => {
    switch (action.type) {
        case constants.UPDATE_SELECTED_CARRIER:
            return action.payload !== undefined ? action.payload : state
        case constants.ADD_LANE_TO_SELECTED_CARRIER:
            const newState = _.clone(state);
            const laneIndex = _.findIndex(newState.lanes, ['id', action.payload.id]);
            newState.lanes[laneIndex >= 0 ? laneIndex : newState.lanes.length] = action.payload;
            return newState;
        case constants.REMOVE_LANE_FROM_SELECTED_CARRIER:
            const clonedState = _.clone(state);
            const lanePosition = _.findIndex(clonedState.lanes, ['id', action.payload]);
            clonedState.lanes.splice(lanePosition, 1);
            return clonedState;
        case constants.ADD_CONTACT_TO_SELECTED_CARRIER:
            const copiedState = _.clone(state);
            const contactIndex = _.findIndex(copiedState.contact, ['id', action.payload.id]);
            copiedState.contact[contactIndex >= 0 ? contactIndex : copiedState.contact.length] = action.payload;
            return copiedState;
        case constants.REMOVE_CONTACT_FROM_SELECTED_CARRIER:
            const stateCopy = _.clone(state);
            const contactPosition = _.findIndex(stateCopy.contact, ['id', action.payload.id]);
            stateCopy.contact.splice(contactPosition, 1);
            return stateCopy;
        default:
            return state
    }
};

export const displaySelectedCarrier = (state = false, action) => {
    switch (action.type) {
        case constants.DISPLAY_SELECTED_CARRIER:
            return action.payload
        default:
            return state
    }
};

