import * as _ from 'lodash';
import constants from '../res/strings/action_constants';

export const laneSearch = (state = { searching: false, searchData: [] }, action) => {
    let lanePosition;

    switch (action.type) {
        case constants.SEARCH_LANES:
            return action.payload;
        case constants.UPDATE_LANE_IN_SEARCH_RESULTS:
            lanePosition = _.findIndex(state.searchData, ['id', action.payload.id]);

            if (action.payload.startLocationTitle !== state.searchData[lanePosition].startLocationTitle ||
                action.payload.endLocationTitle !== state.searchData[lanePosition].endLocationTitle) {
                return removeLaneFromList(state, action.payload.id);
            }

            if (action.payload.flatRate && action.payload.flatRate !== state.flatRate) {
                const clonedState = _.clone(state);
                const updatedLane = action.payload;
                updatedLane.carrier = clonedState.searchData[lanePosition].carrier;
                clonedState.searchData[lanePosition] = updatedLane;
                return clonedState;
            }

            return state;
        case constants.REMOVE_LANE_FROM_SEARCH_RESULTS:
            return removeLaneFromList(state, action.payload);
        default:
            return state;
    }
}

export const carrierSearch = (state = { searching: false, searchData: [] }, action) => {
    let carrierPosition;

    switch (action.type) {
        case constants.SEARCH_CARRIER:
            return action.payload;
        case constants.UPDATE_CARRIER_IN_SEARCH_RESULTS:
            const clonedState = _.clone(state);
            carrierPosition = _.findIndex(state.searchData, ['id', action.payload.id]);
            clonedState.searchData[carrierPosition] = action.payload;
            return clonedState;
        case constants.REMOVE_CARRIER_FROM_SEARCH_RESULTS:
            const newState = _.clone(state);
            carrierPosition = _.findIndex(state.searchData, ['id', action.payload]);
            newState.searchData.splice(carrierPosition, 1);
            return newState;
        default:
            return state;
    }
}

const removeLaneFromList = (state, id) => {
    const newState = _.clone(state);
    const lanePosition = _.findIndex(state.searchData, ['id', id]);
    newState.searchData.splice(lanePosition, 1);
    return newState;
}
