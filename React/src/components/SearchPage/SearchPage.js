import React, { Component } from 'react';
import { Grid, Tab, Segment } from 'semantic-ui-react'

import LaneModal from '../Modals/LaneModalv2';
import ContactModal from '../Modals/ContactModalv2';
import NewCarrierModal from '../Modals/CarrierModalv2';


import LaneSearchTab from './components/SearchPanel/LaneSearchTab';
import CarrierSearchTab from './components/SearchPanel/CarrierSearchTab';

import LaneSearchResults from './components/SearchResults/LaneSearchResults';
import CarrierSearchResults from './components/SearchResults/CarrierSearchResults';

import CarrierDetails from './components/CarrierDetails/CarrierDetails';

import { showSelectedCarrier, hideSelectedCarrier, getCarrierById } from '../../actions/carrier-action-index';

import searchConstants from '../../res/strings/search_constants';

import { connect } from 'react-redux';

export class SearchPage extends Component {
    displaySelectedCarrier(carrierId) {

        const sucessCallback = () => {
            this.props.showSelectedCarrier();
        }

        // getCarrier also sets selected carrier. 
        // That way showSelectedCarrier has a carrier to show.
        this.props.getCarrier(carrierId, sucessCallback)
    }

    renderResults() {
        const { searchType } = this.props;
        const { SEARCH_LANE } = searchConstants;
        return searchType === SEARCH_LANE ?
            <LaneSearchResults
                onResultSelected={(carrierId) => this.displaySelectedCarrier(carrierId)}
            />
            :
            <CarrierSearchResults
                onResultSelected={(carrierId) => this.displaySelectedCarrier(carrierId)}
            />
    }

    renderCarrierDetails() {
        return <CarrierDetails backToResults={() => { this.props.hideSelectedCarrier() }} />
    }

    render() {
        const { showCarrierModal, showLaneModal, showContactModal, displayCarrierDetails } = this.props;
        const panes = [
            { menuItem: 'Lanes', render: () => <LaneSearchTab style={{ height: '100%' }} /> },
            { menuItem: 'Carriers', render: () => <CarrierSearchTab style={{ hieght: '100%' }} /> }
        ]

        return (
            <div style={{ height: '100%', overflow: 'hidden' }} >
                {showContactModal ? <ContactModal /> : null}
                {showLaneModal ? <LaneModal /> : null}
                {showCarrierModal ? <NewCarrierModal /> : null}
                <Grid columns='equal' padded style={{ height: '100%' }}>
                    <Grid.Column width={4}>
                        <Segment style={{ height: '100%' }}>
                            <Tab menu={{ secondary: true, pointing: true, widths: 2 }} panes={panes} />
                        </Segment>
                    </Grid.Column>
                    <Grid.Column >
                        {displayCarrierDetails ? this.renderCarrierDetails() : this.renderResults()}
                    </Grid.Column>
                </Grid >
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        searchType: state.searchType,
        selectedCarrier: state.selectedCarrier,
        showCarrierModal: state.newCarrierModal.display,
        showLaneModal: state.newLaneModal.display,
        showContactModal: state.newContactModal.display,
        displayCarrierDetails: state.displayCarrierDetails,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getCarrier: (id, callback) => {
            dispatch(getCarrierById(id, callback));
        },
        showSelectedCarrier: () => {
            dispatch(showSelectedCarrier());
        },
        hideSelectedCarrier: () => {
            dispatch(hideSelectedCarrier());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);
