import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Tab, Header, Icon, Divider, Form, Input, Button, Segment, ButtonGroup } from 'semantic-ui-react'

import { showCarrierModal } from '../../../../actions/modal-action-index';
import { searchForCarrier } from '../../../../actions/carrier-action-index';
import searchConstants from '../../../../res/strings/search_constants';

import { processCarrierSpreadsheet } from '../../../../actions/file-upload-action-index';

export class CarrierSearchTab extends Component {
    constructor(props) {
        super(props);

        this.toggleEnum = {
            NAME: 0,
            MC: 1,
            ODOT: 2,
        }

        this.state = {
            toggle: {
                name: true,
                mc: false,
                odot: false,
            },
            searchValue: '',
            searchType: 0,
            file: null
        }

        this.fileInputRef = React.createRef();

        this.searchTypeList = [searchConstants.SEARCH_NAME, searchConstants.SEARCH_MC, searchConstants.SEARCH_ODOT];
    }


    handleToggleClick(toggleEnumValue) {
        this.setState({
            toggle: {
                name: toggleEnumValue === this.toggleEnum.NAME ? true : false,
                mc: toggleEnumValue === this.toggleEnum.MC ? true : false,
                odot: toggleEnumValue === this.toggleEnum.ODOT ? true : false,
            },
            searchType: toggleEnumValue
        });
    }

    handleSearchValueUpdate(value) {
        this.setState({
            searchValue: value
        });
    }

    dispatchCarrierSearch() {
        const { searchType, searchValue } = this.state;

        if (searchValue !== '') {
            this.props.searchForCarrier(this.searchTypeList[searchType], searchValue);
        }
    }

    render() {
        const { toggle, searchValue } = this.state;

        return (
            <Tab.Pane style={{ height: '100%', width: '100%', paddingLeft: '0', paddingRight: '0', paddingTop: 0, marginBottom: '1rem' }} basic attached={false} >

                <Segment style={{ marginTop: 0 }} >
                    <Header textAlign={'center'} dividing >
                        <Icon name='search' size='tiny' />
                        Search
                </Header>

                    <Segment basic >
                        <ButtonGroup widths={3} >
                            <Button
                                toggle
                                active={toggle.name}
                                onClick={() => { this.handleToggleClick(this.toggleEnum.NAME) }}
                            >
                                Name
                    </Button>
                            <Button.Or />
                            <Button
                                toggle
                                active={toggle.mc}
                                onClick={() => { this.handleToggleClick(this.toggleEnum.MC) }}
                            >
                                MC#
                    </Button>
                            <Button.Or />
                            <Button
                                toggle
                                active={toggle.odot}
                                onClick={() => { this.handleToggleClick(this.toggleEnum.ODOT) }}
                            >
                                ODOT#
                    </Button>
                        </ButtonGroup>

                        <Form onSubmit={() => { this.dispatchCarrierSearch() }}>
                            <div className='component-vertical-padding'>
                                <Header content={'Search: '} size='tiny' />
                                <Input fluid placeholder={`Name, MC#, or ODOT# `} >
                                    <input
                                        onChange={(event) => { this.handleSearchValueUpdate(event.target.value) }}
                                        value={searchValue}
                                    />
                                </Input>
                            </div>

                            <Segment basic textAlign='center' >
                                <Button
                                    fluid
                                    primary
                                    icon='search'
                                    size='large'
                                    content='Search'
                                    disabled={!(searchValue !== '')}
                                    onClick={() => { this.dispatchCarrierSearch() }} />
                            </Segment>
                        </Form>
                    </Segment>
                </Segment>

                <Divider hidden fitted />

                <Segment >
                    <Header textAlign={'center'} dividing >
                        <Icon name='add' size='tiny' />
                        New Carrier
                    </Header>

                    <Segment placeholder vertical textAlign={'center'} style={{ marginTop: '10px !important' }} >

                        <Segment basic style={{ paddingTop: '0px' }} >
                            <Button primary fluid icon={'add'} content={'New Carrier'} onClick={this.props.showCarrierModal} />
                        </Segment>

                        <Segment basic style={{ paddingTop: '0px' }} >
                            <Divider horizontal >Or</Divider>
                        </Segment>

                        <Segment basic style={{ paddingTop: '0px' }} >
                            <Header>
                                Upload a Spreadsheet
                        </Header>
                            <Segment basic style={{ paddingTop: '0px', paddingBottom: '0px' }} >
                                <Button
                                    primary
                                    fluid
                                    icon={'upload'}
                                    content="Upload"
                                    onClick={() => { this.fileInputRef.current.click() }}
                                />
                                <input
                                    ref={this.fileInputRef}
                                    type="file"
                                    hidden
                                    onChange={(e) => { this.props.uploadCarrierSpreadsheet(e) }}
                                />
                            </Segment>
                        </Segment>
                    </Segment>
                </Segment>
            </Tab.Pane >
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        searchForCarrier: (type, query) => {
            dispatch(searchForCarrier(type, query));
        },
        showCarrierModal: () => {
            dispatch(showCarrierModal());
        },
        uploadCarrierSpreadsheet: (e) => {
            processCarrierSpreadsheet(e, dispatch);
        }
    }
}

export default connect(null, mapDispatchToProps)(CarrierSearchTab);
