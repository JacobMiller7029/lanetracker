import React from 'react';
import { shallow } from 'enzyme';
import { CarrierSearchTab } from './CarrierSearchTab';

describe('Tests CarrierSearchTab component', () => {
    it('CarrierSearchTab component renders successfully', () => {
        const props = {
            showCarrierModal: false,
            showCarrierModal: () => {},
            uploadCarrierSpreadsheet: () => {},
            searchForCarrier: () => {}
        }

        shallow(<CarrierSearchTab {...props} />);
    });       
});