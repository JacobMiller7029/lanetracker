import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Tab, Header, Icon, Checkbox, Divider, Input, Button, Segment, Form } from 'semantic-ui-react'

import GeoSearchBox from './GeoSearchBox';
import { searchForLane } from '../../../../actions/lane-action-index';

const defaultSearchRadius = 50;

export class LaneSearchTab extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pickup: null,
            dropoff: null,
            bidirectional: false,
            searchRadius: defaultSearchRadius,
        }

        this.handleBidirectionalUpdateCheckbox.bind(this);
    }

    handleLocationSelection(isPickup, location) {
        if (isPickup) {
            this.setState({ pickup: location.place_id })
        } else {
            this.setState({ dropoff: location.place_id });
        }
    }

    handleSearchRadiusUpdate(searchRadius) {
        this.setState({ searchRadius });
    }

    handleBidirectionalUpdateCheckbox() {
        this.setState({ bidirectional: !this.state.bidirectional });
    }

    handleSearchSubmit() {
        const { pickup, dropoff } = this.state;

        if (pickup !== null && dropoff !== null) {
            this.props.dispatchLaneSearch(this.state)
        }
    }

    clearSearch() {
        this.setState({
            pickup: null,
            dropoff: null,
            bidirectional: false,
            searchRadius: defaultSearchRadius,
        })
    }

    render() {
        const { bidirectional, pickup, dropoff } = this.state;

        return (
            <Tab.Pane style={{ height: '100%' }} attached={false} >
                <Header textAlign={'center'} dividing >
                    <Icon name='search' size='tiny' />
                    Search
                </Header>
                <GeoSearchBox
                    handleLocationSelect={(location) => { this.handleLocationSelection(true, location) }}
                    header={"Pickup location:"}
                />
                <GeoSearchBox
                    handleLocationSelect={(location) => { this.handleLocationSelection(false, location) }}
                    header={"Delivery location:"}
                />
                <Divider />

                <Form onSubmit={() => { this.handleSearchSubmit() }}>
                    <div className='component-vertical-padding'>
                        <Header content={'Search Radius'} size='tiny' />
                        <Input type='number' fluid placeholder={`Search radius: `} >
                            <input
                                onChange={(event) => { this.handleSearchRadiusUpdate(event.target.value) }}
                                value={this.state.searchRadius}
                            />
                        </Input>
                    </div>
                    <Checkbox toggle checked={bidirectional} onChange={() => { this.handleBidirectionalUpdateCheckbox() }} label='Make search bidirectional' />

                    <Divider />

                    <Segment basic textAlign='center' >
                        <Button
                            fluid
                            primary
                            icon='search'
                            size='large'
                            content='Search'
                            disabled={!(pickup !== null && dropoff !== null)}
                            onClick={() => { this.handleSearchSubmit() }} />
                    </Segment>
                </Form>

            </Tab.Pane >
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatchLaneSearch: (request) => {
            dispatch(searchForLane(request));
        }
    }
}

export default connect(null, mapDispatchToProps)(LaneSearchTab);
