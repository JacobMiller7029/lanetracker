import React from 'react';
import { shallow } from 'enzyme';
import GeoSearchBox from './GeoSearchBox';

describe('Tests GeoSearchBox component', () => {
    it('GeoSearchBox component renders successfully', () => {
        const props = {
            iconState: 1,
            inputValue: '',
            handleLocationSelect: () => {},
            isInputValid: () => {},
            searchForCarrier: () => {}
        }

        shallow(<GeoSearchBox {...props} />);
    });       
});