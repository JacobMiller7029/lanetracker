import _ from 'lodash'
import React from 'react'
import { Search, Label, Segment, Header, Icon } from 'semantic-ui-react'
import GooglePlacesApi from "../../../../api/GooglePlacesApi"

export default class GeoSearchBox extends React.Component {
    constructor(props) {
        super(props);
        this.places = new GooglePlacesApi({});

        this.state = {
            validStatus: props.iconState,
            isLoading: false,
            results: [],
            value: this.props.inputValue,
        }

        this.errorMessage = 'Must select a location';

        this.resultStatusIcon = [
            { icon: 'point', color: 'black' },
            { icon: 'exclamation circle', color: 'red' },
            { icon: 'check circle outline', color: 'green' }
        ]
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.value.length > 0 && this.state.value.length === 0) {
            return this.resetComponent()
        }
    }

    resetComponent = () => {
        this.setState({
            validStatus: 0,
            isLoading: false,
            results: [],
            value: this.props.inputValue,
        })
    }

    handleResultSelect = (e, { result }) => {
        this.setState({ value: result.description, validStatus: 2 });
        this.props.handleLocationSelect(result);
        this.props.isInputValid(true);
    }

    handleSearchChange = (e, { value }) => {
        if (value.length >= 3) {
            this.setState({ isLoading: true, value, validStatus: 0 })
            this.props.isInputValid(false);

            let searchResult = this.places.queryForPlace(value);

            searchResult.then((response) => {
                if (response.status === 200) {
                    this.setState({
                        isLoading: false,
                        results: response.data.predictions,
                    });
                } else {
                    this.setState({
                        isLoading: false,
                    });
                }
            }).catch((e) => {
            })
        } else {
            this.setState({ value });
        }
    }

    handleOnBlur = () => {
        const { validStatus } = this.state;

        if (validStatus !== 2) {
            this.setState({ validStatus: 1 })
        }
    }

    render() {
        const { isLoading, value, results, validStatus } = this.state
        const { header } = this.props;

        const resultRenderer = ({ description, place_id }) => <Label key={place_id} content={description} />
        const displayErrorMessage = (validStatus === 1);

        return (
            <Segment style={{ paddingLeft: '0', paddingRight: '0' }} basic>
                <Header
                    as='h6'
                    icon={
                        <Icon
                            name={this.resultStatusIcon[validStatus].icon}
                            color={this.resultStatusIcon[validStatus].color}
                        />}
                    content={header}
                    size='tiny'
                />
                <Search
                    onBlur={() => { this.handleOnBlur() }}
                    loading={isLoading}
                    onResultSelect={this.handleResultSelect}
                    onSearchChange={_.debounce(this.handleSearchChange, 500, { leading: true })}
                    results={results}
                    value={value}
                    resultRenderer={resultRenderer}
                />
                {displayErrorMessage ?
                    <Label
                        active={true}
                        color='red'
                        content={this.errorMessage}
                        pointing='above'
                        size='small'
                    />
                    :
                    null
                }
            </Segment>
        )
    }
}


GeoSearchBox.defaultProps = {
    iconState: 0,
    inputValue: '',
    isInputValid: () => { }
}