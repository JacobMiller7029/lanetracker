import React from 'react';
import { shallow } from 'enzyme';
import { LaneSearchTab } from './LaneSearchTab';

describe('Tests LaneSearchTab component', () => {
    it('LaneSearchTab component renders successfully', () => {
        const props = {
            dispatchLaneSearch: () => {}
        }

        shallow(<LaneSearchTab {...props} />);
    });       
});