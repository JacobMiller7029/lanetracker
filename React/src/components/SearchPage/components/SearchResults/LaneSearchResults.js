import _ from 'lodash'
import { connect } from 'react-redux';
import React, { Component } from 'react'
import { Segment, Table, Header, Icon, Message } from 'semantic-ui-react'

import TableHeader from './LaneSearchResultsTable/LaneTableHeader';
import TableBody from './LaneSearchResultsTable/LaneTableBody';

export class LaneSearchResults extends Component {
    constructor(props) {
        super(props);

        this.state = {
            firstLoad: true,
        }
    }

    componentWillMount() {
        this.resetSearchState();
    }

    resetSearchState() {
        this.setState({
            column: null,
            data: this.props.searchResponse.searchData,
            direction: 'ascending',
        })
    }

    componentDidUpdate(prevProps, prevState) {
        const { firstLoad } = this.state;
        const { searchData, searching } = this.props.searchResponse;

        if (firstLoad) {
            this.setState({ firstLoad: false });
        }

        //Initial search here, Will go into here. Most subsquent searchData updates will bypass this.
        if (!searching && prevState.data.length === 0 && prevProps.searchResponse.searchData.length === 0 && searchData.length !== 0) {
            this.setState({ data: searchData, firstLoad: false });
            return;
        }

        // Used to reset the search data. By passing an empty searchData object, it'll reset the search results.
        if (prevState.data.length !== 0 && prevProps.searchResponse.searchData.length !== 0 && searchData.length === 0) {
            this.resetSearchState();
            return;
        }
    }

    handleRowClick(carrierId) {
        this.props.onResultSelected(carrierId);
    }

    handleSort = clickedColumn => () => {
        const { column, direction, data } = this.state;

        if (column !== clickedColumn) {


            this.setState({
                column: clickedColumn,
                data: _.sortBy(data, [clickedColumn]),
                direction: 'ascending',
            })

            return;
        }

        this.setState({
            data: data.reverse(),
            direction: direction === 'ascending' ? 'descending' : 'ascending',
        })
    }

    renderHeader() {
        return (
            <Header textAlign={'center'} dividing >
                <Icon name='database' size='tiny' />
                Results
            </Header>
        )
    }

    renderTable() {
        const { column, data, direction } = this.state;
        return (
            <Table striped sortable singleLine fixed selectable size='large' >
                <TableHeader handleSort={this.handleSort} direction={direction} column={column} />
                <TableBody onRowClicked={(carrier) => { this.handleRowClick(carrier) }} data={data} />
            </Table>
        )
    }

    renderNoResults() {
        return (
            <Message
                icon='search'
                header='No results'
                content='Consider increasing the search radius'
            />
        );
    }

    render() {
        const { data, firstLoad } = this.state;
        const { searching } = this.props.searchResponse;

        return (
            <Segment loading={searching} style={{ height: '100%' }} >
                {this.renderHeader()}
                {data.length > 0 ? this.renderTable() : ((searching || firstLoad) ? null : this.renderNoResults())}
            </Segment>
        )
    }
}

const mapStateToProps = state => {
    return {
        searchResponse: state.laneSearchResults,
        searchInProgress: state.laneSearchInProgress
    };
}

export default connect(mapStateToProps)(LaneSearchResults);