import React from 'react';
import { shallow } from 'enzyme';
import CarrierTableHeader from './CarrierTableHeader';

describe('Tests CarrierTableHeader component', () => {
    it('CarrierTableHeader component renders successfully', () => {

        const props = {
            column: '',
            direction: '',
            handleSort: () => {}
        }

        shallow(<CarrierTableHeader {...props} />);
    });       
});
