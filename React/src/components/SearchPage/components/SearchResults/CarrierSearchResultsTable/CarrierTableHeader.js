import React from 'react'
import { Table } from 'semantic-ui-react'

export default (props) => {
    return (
        <Table.Header>
            <Table.Row>
                <Table.HeaderCell
                    sorted={props.column === 'carrier' ? props.direction : null}
                    onClick={props.handleSort('title')}
                >
                    Carrier
            </Table.HeaderCell>
                <Table.HeaderCell
                    sorted={props.column === 'from' ? props.direction : null}
                    onClick={props.handleSort('homeCity')}
                >
                    Operates from
            </Table.HeaderCell>
                <Table.HeaderCell
                    sorted={props.column === 'to' ? props.direction : null}
                    onClick={props.handleSort('endCity')}
                >
                    Notes
            </Table.HeaderCell>

            </Table.Row>
        </Table.Header>
    )
}