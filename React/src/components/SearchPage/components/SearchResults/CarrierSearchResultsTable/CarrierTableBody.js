import _ from 'lodash'
import React from 'react'
import { Table } from 'semantic-ui-react'

export default (props) => {
    return (
        <Table.Body>
            {_.map(props.data, (singleResult) => {

                const { id, title, homeCity, notes } = singleResult;

                const selectRow = () => {
                    props.onRowClicked(id);
                }

                return (
                    <Table.Row onClick={() => { selectRow() }} key={id}>
                        <Table.Cell >{title}</Table.Cell>
                        <Table.Cell >{homeCity}</Table.Cell>
                        <Table.Cell >{notes}</Table.Cell>
                    </Table.Row>
                )
            }
            )}
        </Table.Body>
    )
}