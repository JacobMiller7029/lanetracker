import React from 'react';
import { shallow } from 'enzyme';
import CarrierTableBody from './CarrierTableBody';

describe('Tests CarrierTableBody component', () => {
    it('CarrierTableBody component renders successfully', () => {
        const singleItem = {
            id: 0, 
            title: '', 
            homeCity: '', 
            notes: ''
        }

        const props = {
            data: [singleItem],
            onRowClicked: () => {}
        }

        shallow(<CarrierTableBody {...props} />);
    });       
});
