import React from 'react';
import { shallow } from 'enzyme';
import { LaneSearchResults } from './LaneSearchResults';

describe('Tests LaneSearchResults component', () => {
    it('LaneSearchResults component renders successfully', () => {
        const props = {
            searchResponse: {
                searching: false,
                searchData: []
            },
            onResultSelected: () => {}
        }

        shallow(<LaneSearchResults {...props} />);
    });       
});