import React from 'react';
import { shallow } from 'enzyme';
import LaneTableBody from './LaneTableBody';

describe('Tests LaneTableBody component', () => {
    it('LaneTableBody component renders successfully', () => {
        const singleItem = {
            carrier: {}, 
            id: 0, 
            startLocationTitle: '', 
            endLocationTitle: '', 
            flatRate: 0
        }

        const props = {
            data: [singleItem],
            onRowClicked: () => {}
        }

        shallow(<LaneTableBody {...props} />);
    });       
});