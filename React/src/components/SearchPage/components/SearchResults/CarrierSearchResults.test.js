import React from 'react';
import { shallow } from 'enzyme';
import { CarrierSearchResults } from './CarrierSearchResults';

describe('Tests CarrierSearchResults component', () => {
    it('CarrierSearchResults component renders successfully', () => {
        const props = {
            searchResponse: {
                searching: false,
                searchData: []
            },
            onResultSelected: () => {}
        }

        shallow(<CarrierSearchResults {...props} />);
    });       
});