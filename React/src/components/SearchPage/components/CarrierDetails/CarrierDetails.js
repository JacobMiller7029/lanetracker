import _ from 'lodash'
import React, { Component } from 'react';
import {
    Breadcrumb,
    Segment,
    Divider,
    Button,
    Header,
    Table,
    Icon,
    Grid,
    List,
} from 'semantic-ui-react'

import LaneTableHeader from './lanes/LaneTableHeader'
import LaneTableBody from './lanes/LaneTableBody';

import ContactTableHeader from './contacts/ContactTableHeader';
import ContactTableBody from './contacts/ContactTableBody';

import { showLaneModal, showContactModal, showCarrierModal } from '../../../../actions/modal-action-index';

import { connect } from 'react-redux';

export class CarrierDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contacts: {
                column: null,
                data: this.props.selectedCarrier.contact,
                direction: 'ascending',
            },
            lanes: {
                column: null,
                data: this.props.selectedCarrier.lanes,
                direction: 'ascending',
            },
            showLaneEditPanel: false
        }
    }

    handleCarrierEditClick() {
        this.props.displayCarrierModal(this.props.selectedCarrier);
    }

    handleContactRowClick(contact) {
        this.props.displayContactModal(contact);
    }

    handleLaneRowClick(lane) {
        this.props.displayLaneModal(lane);
    }

    handleContactSort = clickedColumn => () => {
        const { column, direction, data } = this.state.contacts;

        if (column !== clickedColumn) {
            this.setState({
                contacts: {
                    column: clickedColumn,
                    data: _.sortBy(data, [clickedColumn]),
                    direction: 'ascending',
                }
            })
            return;
        }

        this.setState({
            contacts: {
                data: data.reverse(),
                direction: direction === 'ascending' ? 'descending' : 'ascending',
            }
        })
    }

    handleLaneSort = clickedColumn => () => {
        const { column, direction, data } = this.state.lanes;
        if (column !== clickedColumn) {
            this.setState({
                lanes: {
                    column: clickedColumn,
                    data: _.sortBy(data, [clickedColumn]),
                    direction: 'ascending',
                }
            })

            return;
        }

        this.setState({
            lanes: {
                data: data.reverse(),
                direction: direction === 'ascending' ? 'descending' : 'ascending',
            }
        })
    }

    renderCarrierDetails() {

        const { showLaneEditPanel } = this.state;

        return (
            <Grid.Column
                onMouseOver={() => {
                    this.setState({
                        showLaneEditPanel: true,
                    })
                }}
                onMouseLeave={() => {
                    this.setState({
                        showLaneEditPanel: false
                    })
                }}
                verticalAlign='top'
                stretched
            >
                <Header as='h2' textAlign={'center'} dividing >
                    <Header.Content >
                        <Icon name='truck' size='small' />
                        Carrier Details
                    </Header.Content>
                </Header>

                {showLaneEditPanel ?
                    <Button style={{ position: 'absolute', top: '0', right: '0', marginRight: '1rem' }}
                        icon={'edit'}
                        // floated='right'
                        size='small'
                        onClick={() => { this.handleCarrierEditClick() }}
                        content='Edit'
                        compact
                    />
                    :
                    null
                }

                <Divider hidden />

                <Grid as={Segment} basic textAlign={'left'} >
                    <Grid.Row textAlign='left' columns={2} verticalAlign='top'>
                        <Grid.Column >
                            <List size={'large'} >
                                <List.Item>
                                    <List.Icon name='point' />
                                    <List.Content>Operates out of {this.props.selectedCarrier.homeCity || 'N/A'}</List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Icon name='hashtag' />
                                    <List.Content>MC#: {this.props.selectedCarrier.mcNum || 'N/A'}</List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Icon name='hashtag' />
                                    <List.Content>USDOT#: {this.props.selectedCarrier.odotNum || 'N/A'}</List.Content>
                                </List.Item>
                            </List>
                        </Grid.Column>

                        <Grid.Column >
                            Equitment types: coming soon
                        {/* <List size={'large'} bulleted>
                                <List.Item>
                                    <List.Content>Flatbed</List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Content>Dryvan</List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Content>Reefer</List.Content>
                                </List.Item>
                            </List>*/}
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row textAlign='left' columns={1} verticalAlign='top'>
                        <Grid.Column>
                            <Header as='h2' dividing >Notes</Header>
                            <p>
                                {this.props.selectedCarrier.notes}
                            </p>
                        </Grid.Column>
                    </Grid.Row>

                </Grid>
            </Grid.Column >
        )
    }

    renderContacts() {
        return (
            <Grid.Column stretched verticalAlign='top' >
                <Header as='h2' textAlign={'center'} dividing >
                    <Header.Content>
                        <Icon name='address book outline' size='small' />
                        Contacts
                    </Header.Content>
                    <Button
                        style={{ position: 'absolute', top: '0', right: '0', marginRight: '1rem' }}
                        primary
                        compact
                        icon={'add'}
                        content={'Contact'}
                        floated='right'
                        size='tiny'
                        onClick={this.props.displayContactModal}
                    />
                </Header>
                {this.renderContactTable()}
            </Grid.Column>
        )
    }

    renderContactTable() {
        const { column, data, direction } = this.state.contacts;

        return (
            <Table striped sortable singleLine fixed selectable size='small' >
                <ContactTableHeader handleSort={this.handleContactSort} direction={direction} column={column} />
                <ContactTableBody onRowClicked={(contact) => { this.handleContactRowClick(contact) }} data={data} />
            </Table>
        )
    }

    renderLaneTable() {
        const { column, data, direction } = this.state.lanes;

        return (
            <Table striped sortable singleLine fixed selectable size='small' >
                <LaneTableHeader handleSort={this.handleLaneSort} direction={direction} column={column} />
                <LaneTableBody onRowClicked={(lane) => { this.handleLaneRowClick(lane) }} data={data} />
            </Table>
        )
    }

    renderLaneList() {
        return (
            <Grid.Column stretched verticalAlign='top'>
                <Header
                    as='h2'
                    textAlign={'center'}
                    dividing
                >
                    <Header.Content>
                        <Icon name='browser' size='small' />
                        Lanes
                    </Header.Content>
                    <Button
                        style={{ position: 'absolute', top: '0', right: '0', marginRight: '1rem' }}
                        primary
                        icon={'add'}
                        content={'Lane'}
                        floated='right'
                        size='tiny'
                        compact
                        onClick={this.props.displayLaneModal}
                    />
                </Header>
                {this.renderLaneTable()}
            </Grid.Column>
        )
    }

    render() {
        return (
            <Segment style={{ height: '100%', 'maxHeight': '100vh', 'overflowY': 'scroll' }} >

                <Breadcrumb size={'tiny'} >
                    <Breadcrumb.Section onClick={this.props.backToResults}>Search</Breadcrumb.Section>
                    <Breadcrumb.Divider icon='right chevron' />
                    <Breadcrumb.Section >Carrier Details</Breadcrumb.Section>
                    <Breadcrumb.Divider icon='right arrow' />
                    <Breadcrumb.Section active>{this.props.selectedCarrier.title}</Breadcrumb.Section>
                </Breadcrumb>

                <Divider hidden />

                <Grid textAlign={'center'} >
                    <Grid.Row stretched textAlign='center' columns={2} >
                        {this.renderCarrierDetails()}
                        {this.renderContacts()}
                    </Grid.Row>

                    <Grid.Row stretched textAlign='center' columns={1} >
                        {this.renderLaneList()}
                    </Grid.Row>
                </Grid>
            </Segment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        displayCarrierModal: (carrier) => {
            dispatch(showCarrierModal(carrier));
        },
        displayLaneModal: (lane = null) => {
            dispatch(showLaneModal(lane));
        },
        displayContactModal: (contact = null) => {
            dispatch(showContactModal(contact));
        }
    }
}

const mapStateToProps = state => {
    return {
        selectedCarrier: state.selectedCarrier,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CarrierDetails);
