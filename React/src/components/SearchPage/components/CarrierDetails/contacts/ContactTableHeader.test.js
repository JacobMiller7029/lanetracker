import React from 'react';
import { shallow } from 'enzyme';
import ContactTableHeader from './ContactTableHeader';

describe('Tests ContactTableHeader component', () => {
    it('ContactTableHeader component renders successfully', () => {
        const props = {
            column: '',
            direction: '',
            handleSort: () => {}
        }

        shallow(<ContactTableHeader { ...props } />);
    });       
});