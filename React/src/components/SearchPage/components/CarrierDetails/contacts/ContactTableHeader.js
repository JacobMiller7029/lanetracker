import React from 'react'
import { Table } from 'semantic-ui-react'

export default (props) => {
    return (
        <Table.Header>
            <Table.Row>
                <Table.HeaderCell
                    sorted={props.column === 'name' ? props.direction : null}
                    onClick={props.handleSort('name')}
                >
                    Name
            </Table.HeaderCell>
                <Table.HeaderCell
                    sorted={props.column === 'phoneNum' ? props.direction : null}
                    onClick={props.handleSort('phoneNum')}
                >
                    Phone #:
            </Table.HeaderCell>
                <Table.HeaderCell
                    sorted={props.column === 'email' ? props.direction : null}
                    onClick={props.handleSort('email')}
                >
                    Email
            </Table.HeaderCell>
                <Table.HeaderCell
                    sorted={props.column === 'faxNum' ? props.direction : null}
                    onClick={props.handleSort('faxNum')}
                >
                    Fax Number
            </Table.HeaderCell>

            </Table.Row>
        </Table.Header>
    )
}