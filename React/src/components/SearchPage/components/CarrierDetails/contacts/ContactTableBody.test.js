import React from 'react';
import { shallow } from 'enzyme';
import ContactTableBody from './ContactTableBody';

describe('Tests ContactTableBody component', () => {
    it('ContactTableBody component renders successfully', () => {
        const props = {
            column: '',
            direction: '',
            handleSort: () => {}
        }

        shallow(<ContactTableBody { ...props } />);
    });       
});