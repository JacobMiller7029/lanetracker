import _ from 'lodash'
import React from 'react'
import { Table } from 'semantic-ui-react'

export default (props) => {
    return (
        <Table.Body>
            {_.map(props.data, (singleResult) => {
                const { id, name, phoneNum, email, faxNum } = singleResult;

                const selectRow = () => {
                    props.onRowClicked(singleResult);
                }

                return (
                    <Table.Row onClick={() => { selectRow() }} key={id}>
                        <Table.Cell >{name}</Table.Cell>
                        <Table.Cell >{phoneNum}</Table.Cell>
                        <Table.Cell >{email}</Table.Cell>
                        <Table.Cell >{faxNum}</Table.Cell>
                    </Table.Row>
                )
            }
            )}
        </Table.Body>
    )
}