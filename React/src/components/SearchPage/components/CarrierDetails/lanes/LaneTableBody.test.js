import React from 'react';
import { shallow } from 'enzyme';
import LaneTableBody from './LaneTableBody';

describe('Tests LaneTableBody component', () => {
    it('LaneTableBody component renders successfully', () => {
        const props = {
            column: '',
            direction: '',
            handleSort: () => {}
        }

        shallow(<LaneTableBody { ...props } />);
    });       
});