import React from 'react';
import { shallow } from 'enzyme';
import LaneTableHeader from './LaneTableHeader';

describe('Tests LaneTableHeader component', () => {
    it('LaneTableHeader component renders successfully', () => {
        const props = {
            column: '',
            direction: '',
            handleSort: () => {}
        }

        shallow(<LaneTableHeader { ...props } />);
    });       
});