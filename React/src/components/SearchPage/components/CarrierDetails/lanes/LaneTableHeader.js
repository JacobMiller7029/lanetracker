import React from 'react'
import { Table } from 'semantic-ui-react'

export default (props) => {
    return (
        <Table.Header>
            <Table.Row>
                <Table.HeaderCell
                    sorted={props.column === 'from' ? props.direction : null}
                    onClick={props.handleSort('startCity')}
                >
                    From
                </Table.HeaderCell>
                <Table.HeaderCell
                    sorted={props.column === 'to' ? props.direction : null}
                    onClick={props.handleSort('endCity')}
                >
                    To
                </Table.HeaderCell>
                <Table.HeaderCell
                    sorted={props.column === 'rate' ? props.direction : null}
                    onClick={props.handleSort('flatRate')}
                >
                    Rate
                </Table.HeaderCell>
            </Table.Row>
        </Table.Header>
    )
}
