import _ from 'lodash'
import React from 'react'
import { Table } from 'semantic-ui-react'

export default (props) => {
    return (
        <Table.Body>
            {_.map(props.data, (singleResult) => {
                const { id, startLocationTitle, endLocationTitle, flatRate } = singleResult;

                const selectRow = () => {
                    props.onRowClicked(singleResult);
                }

                return (
                    <Table.Row onClick={() => { selectRow() }} key={id}>
                        <Table.Cell >{startLocationTitle}</Table.Cell>
                        <Table.Cell >{endLocationTitle}</Table.Cell>
                        <Table.Cell >{flatRate}</Table.Cell>
                    </Table.Row>
                )
            }
            )}
        </Table.Body>
    )
}