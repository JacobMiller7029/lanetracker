import React from 'react';
import { shallow } from 'enzyme';
import { CarrierDetails } from './CarrierDetails';

describe('Tests CarrierDetails component', () => {
    const selectedCarrier = {
        title: '',
        homeCity: '',
        mcNum: '',
        odotNum: '',
        contact: {},
        lanes: [],
        notes: ''
    }

    it('CarrierDetails component renders successfully', () => {
        shallow(<CarrierDetails selectedCarrier />);
    });       
});