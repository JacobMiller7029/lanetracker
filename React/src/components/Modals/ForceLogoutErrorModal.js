import React, { Component } from 'react';
import { Button, Modal } from 'semantic-ui-react';
import { hideForcedLogoutModal } from '../../actions/modal-action-index';
import statusEnums from '../../res/enums/AuthorizationStatusEnums';

import { connect } from 'react-redux';

export class ForceLogoutErrorModal extends Component {
    state = {
        displayModal: true
    }

    onClose() {
        this.setState({ displayModal: false })
    }

    render() {
        const { showModal } = this.props;
        const { displayModal } = this.state;
        const renderModal = showModal && displayModal;

        return (
            <Modal
                centered
                open={renderModal}
                closeOnEscape={true}
                closeOnDimmerClick={true}
                onClose={() => { this.onClose() }}
                basic
                size='mini'
            >
                <Modal.Header icon="warning sign" content="You've been logged out." />
                <Modal.Content >
                    <p>
                        Your session has expired. Please log in again to start a new session.
                    </p>
                </Modal.Content>
                <Modal.Actions>
                    <Button onClick={() => { this.onClose() }} basic inverted>
                        Ok
                    </Button>
                </Modal.Actions>
            </Modal >
        );
    }
}

const mapStateToProps = state => {
    return {
        showModal: state.authToken.status === statusEnums.FORCED_LOGOUT,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        hideForcedLogoutModal: () => {
            dispatch(hideForcedLogoutModal());
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ForceLogoutErrorModal);
