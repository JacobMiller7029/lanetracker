import React, { Component } from 'react';
import { Button, Modal } from 'semantic-ui-react';
import { hideLoginErrorModal } from '../../actions/modal-action-index';

import { connect } from 'react-redux';

export class LoginErrorModal extends Component {
    onClose() {
        this.props.hideLoginErrorModal();
    }

    render() {
        const { showModal } = this.props;

        return (
            <Modal
                centered
                open={showModal}
                closeOnEscape={true}
                closeOnDimmerClick={true}
                onClose={() => { this.onClose() }}
                basic
                size='small'
            >
                <Modal.Header icon="warning sign" content="Login failed" />
                <Modal.Content >
                    <p>
                        Invalid password or username.
                    </p>
                </Modal.Content>
                <Modal.Actions>
                    <Button onClick={() => { this.onClose() }} basic color='red' inverted>
                        Ok
                    </Button>
                </Modal.Actions>
            </Modal >
        );
    }
}

const mapStateToProps = state => {
    return {
        showModal: state.displayLoginErrorModal,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        hideLoginErrorModal: () => {
            dispatch(hideLoginErrorModal());
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginErrorModal);
