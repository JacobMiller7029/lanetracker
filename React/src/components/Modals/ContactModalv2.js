import validator from 'validator';
import React, { Component } from 'react';
import { Button, Modal, Form, Header, Label } from 'semantic-ui-react';

import DeleteModal from './DeleteModal';

import { saveContact, updateContact, deleteContact } from '../../actions/contact-action-index';
import { hideContactModal } from '../../actions/modal-action-index';

import { connect } from 'react-redux';


// This class needs drastically better form validation.
export class ContactModal extends Component {
    constructor(props) {
        super(props);

        const displayContent = props.displayContent || {};

        this.state = {
            name: displayContent.name || '',
            phoneNum: displayContent.phoneNum || '',
            email: displayContent.email || '',
            faxNum: displayContent.faxNum || '',

            saveInProgress: false,
            showDeleteModal: false,

            // Assumes data from the backend is valid
            showNameError: false,
            showPhoneError: false,
            showEmailError: false,
            showFaxError: false,
        }
    }

    handleDeleteModal(showDeleteModal = false) {
        this.setState({
            showDeleteModal
        });
    }

    handFormFieldUpdate(field, value) {
        this.setState({ [field]: value });
    }

    onClose() {
        this.props.hideContactModal();
    }

    buildNewContact() {
        const { selectedCarrier } = this.props;
        const displayContent = this.props.displayContent || {};

        const {
            name,
            phoneNum,
            email,
            faxNum,
        } = this.state;

        const contactDto = {
            id: displayContent.id || null,
            carrier: {
                id: selectedCarrier.id
            },
            name: name,
            phoneNum,
            faxNum,
            email
        }

        return contactDto;
    }

    isContactValid() {
        return this.isFieldValid('name') && this.isFieldValid('phoneNum');
    }

    isFieldValid(field) {
        const {
            name,
            phoneNum,
            email,
            faxNum,
        } = this.state;

        if (field === 'name') {
            return (validator.isAlpha(name.replace(/ /g, '')) && !validator.isEmpty(name));
        }

        if (field === 'email') {
            return (validator.isEmail(email) || validator.isEmpty(email));
        }

        if (field === 'phoneNum') {
            return (validator.isMobilePhone(phoneNum, null));
        }

        if (field === 'faxNum') {
            return (validator.isMobilePhone(faxNum, null) || validator.isEmpty(faxNum));
        }

        return false;
    }

    onSaveContact() {
        const { displayContent } = this.props;


        if (this.isContactValid()) {
            this.setState({ saveInProgress: true })

            const successCallback = () => {
                this.setState({ saveInProgress: false });
                this.onClose();
            }

            displayContent ?
                this.props.updateContact(this.buildNewContact(), successCallback)
                :
                this.props.saveNewContact(this.buildNewContact(), successCallback);
        }
    }

    renderDeleteButton() {
        return (
            <Button style={{ position: 'absolute', top: '0', right: '0', marginRight: '1rem', marginTop: '1rem' }}
                icon={'trash'}
                size='large'
                onClick={() => {
                    this.handleDeleteModal(true);
                }}
                content='Delete'
                negative
                compact
            />
        );
    }

    renderDeleteModal() {
        const { displayContent } = this.props;
        return (
            <DeleteModal
                entity={'this contact?'}
                onConfirmDelete={() => {
                    this.props.deleteContact(displayContent.id);
                    this.handleDeleteModal(false);
                    this.props.hideContactModal();
                }}
                onCancel={() => { this.handleDeleteModal(false); }}
            />
        )
    }

    render() {
        const { displayContent } = this.props;
        const {
            name,
            email,
            phoneNum,
            faxNum,
            saveInProgress,
            showDeleteModal,
            showNameError,
            showPhoneError,
            showEmailError,
            showFaxError
        } = this.state;

        const modalTitle = displayContent ? 'Edit Contact' : 'New Contact'

        return (
            <Modal
                centered
                open={true}
                closeOnEscape={true}
                closeOnDimmerClick={true}
                onClose={() => { this.onClose() }}
                size='small'
            >
                <Modal.Header as={Header} textAlign='center' >{modalTitle}</Modal.Header>

                {displayContent ? this.renderDeleteButton() : null}

                <Modal.Content >
                    <Form>
                        <Form.Group widths='equal' >
                            <Form.Field required >
                                <div>
                                    <Form.Input
                                        fluid
                                        required
                                        id='form-subcomponent-shorthand-input-name'
                                        label='Name'
                                        placeholder='name'
                                        value={name}
                                        onBlur={() => {
                                            this.setState({
                                                showNameError: !this.isFieldValid('name')
                                            })
                                        }}
                                        onChange={(e) => { this.handFormFieldUpdate('name', e.target.value); }}
                                    />
                                    {showNameError ?
                                        <Label
                                            active={true}
                                            color='red'
                                            content={'Must enter a name.'}
                                            pointing='above'
                                            size='small'
                                        />
                                        :
                                        null
                                    }
                                </div>
                            </Form.Field>
                            <Form.Field required >
                                <div>
                                    <Form.Input
                                        fluid
                                        id='form-subcomponent-shorthand-input-odot'
                                        label='Email'
                                        placeholder='@'
                                        value={email}
                                        onBlur={() => {
                                            this.setState({
                                                showEmailError: !this.isFieldValid('email')
                                            })
                                        }}
                                        onChange={(e) => { this.handFormFieldUpdate('email', e.target.value); }}
                                    />
                                    {showEmailError ?
                                        <Label
                                            active={true}
                                            color='red'
                                            content={'Email must be in a valid format.'}
                                            pointing='above'
                                            size='small'
                                        />
                                        :
                                        null
                                    }
                                </div>
                            </Form.Field>
                        </Form.Group>
                        <Form.Group widths='equal' >
                            <Form.Field required >
                                <div>
                                    <Form.Input
                                        fluid
                                        required
                                        id='form-subcomponent-shorthand-input-odot'
                                        label='Phone Number'
                                        placeholder='#'
                                        value={phoneNum}
                                        onBlur={() => {
                                            this.setState({
                                                showPhoneError: !this.isFieldValid('phoneNum')
                                            })
                                        }}
                                        onChange={(e) => { this.handFormFieldUpdate('phoneNum', e.target.value); }}
                                    />
                                    {showPhoneError ?
                                        <Label
                                            active={true}
                                            color='red'
                                            content={'Must enter a valid phone number - (###-###-####).'}
                                            pointing='above'
                                            size='small'
                                        />
                                        :
                                        null
                                    }

                                </div>
                            </Form.Field>
                            <Form.Input
                                fluid
                                id='form-subcomponent-shorthand-input-odot'
                                label='Fax number'
                                placeholder='#'
                                value={faxNum}
                                onBlur={() => {
                                    this.setState({
                                        showFaxError: !this.isFieldValid('faxNum')
                                    })
                                }}
                                onChange={(e) => {
                                    this.handFormFieldUpdate('faxNum', e.target.value);
                                }}
                            />
                            {showFaxError ?
                                <Label
                                    active={true}
                                    color='red'
                                    content={'Must enter a valid fax number - (###-###-####).'}
                                    pointing='above'
                                    size='small'
                                />
                                :
                                null
                            }
                        </Form.Group>
                    </Form>
                </Modal.Content>

                <Modal.Actions>
                    {showDeleteModal ? this.renderDeleteModal() : null}
                    <Button onClick={() => { this.onClose() }} >
                        Cancel
                    </Button>
                    <Button
                        onClick={() => { this.onSaveContact(); }}
                        disabled={!this.isContactValid()}
                        positive
                        labelPosition='right'
                        icon='save'
                        content={displayContent ? 'Save' : 'Create'}
                        loading={saveInProgress}
                    />
                </Modal.Actions>
            </Modal>
        );
    }
}

const mapStateToProps = state => {
    return {
        displayContent: state.newContactModal.content.id ? state.newContactModal.content : null,
        selectedCarrier: state.selectedCarrier,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        hideContactModal: () => {
            dispatch(hideContactModal());
        },
        saveNewContact: (newContact, callback) => {
            dispatch(saveContact(newContact, callback));
        },
        updateContact: (contact, callback = () => { }) => {
            dispatch(updateContact(contact, callback));
        },
        deleteContact: (contactId, callback = () => { }) => {
            dispatch(deleteContact(contactId, callback));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactModal);
