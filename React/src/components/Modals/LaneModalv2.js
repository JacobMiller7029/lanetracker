import React, { Component } from 'react';

import { Button, Modal, Form, Header } from 'semantic-ui-react';

import DeleteModal from './DeleteModal';
import GeoSearchBox from '../SearchPage/components/SearchPanel/GeoSearchBox';
import { saveLane, updateLane, deleteLane } from '../../actions/lane-action-index';
import { hideLaneModal } from '../../actions/modal-action-index';

import { connect } from 'react-redux';

export class LaneModal extends Component {
    constructor(props) {
        super(props);
        const displayContent = props.displayContent || {};

        this.state = {
            startLocationTitle: displayContent.startLocationTitle || '',
            startLocationPlaceId: null,
            endLocationTitle: displayContent.endLocationTitle || '',
            endLocationPlaceId: null,
            flatRate: displayContent.flatRate || 0.0,
            saveInProgress: false,
            showDeleteModal: false,
            startLaneValid: displayContent.id ? true : false,
            endLaneValid: displayContent.id ? true : false,
        }
    }

    handleDeleteModal(showDeleteModal = false) {
        this.setState({
            showDeleteModal
        });
    }

    handleGeolocation(location, placeId) {
        this.setState({ [location]: placeId });
    }

    handFormFieldUpdate(field, value) {
        this.setState({ [field]: value });
    }

    onClose() {
        this.props.hideLaneModal();
    }

    buildLane() {
        const { selectedCarrier } = this.props;

        const displayContent = this.props.displayContent || {};

        const {
            startLocationTitle,
            startLocationPlaceId,
            endLocationTitle,
            endLocationPlaceId,
            flatRate
        } = this.state;

        const laneDto = {
            id: displayContent.id || null,
            carrier: {
                id: selectedCarrier.id
            },
            startLocationTitle: startLocationTitle,
            startPlaceId: startLocationPlaceId,
            endLocationTitle: endLocationTitle,
            endPlaceId: endLocationPlaceId,

            // Backend overwrites these if placeId's are not null.
            endLat: displayContent.endLat || 0.0,
            endLong: displayContent.endLong || 0.0,
            startLat: displayContent.startLat || 0.0,
            startLong: displayContent.startLong || 0.0,
            flatRate: flatRate,
        }

        return laneDto;
    }

    isLaneValid() {
        const { startLaneValid, endLaneValid, startLocationPlaceId, endLocationPlaceId, startLocationTitle, endLocationTitle } = this.state;

        // Start location validation
        if (startLocationPlaceId === null && startLocationTitle === '') {
            return false;
        }

        // End location validation
        if (endLocationPlaceId === null && !endLocationTitle === '') {
            return false;
        }

        return (startLaneValid && endLaneValid);
    }

    onSaveLane() {
        const { displayContent } = this.props;

        if (this.isLaneValid()) {
            this.setState({ saveInProgress: true })

            const successCallback = (lane) => {
                this.setState({ saveInProgress: false });
                this.onClose();
            }

            displayContent ?
                this.props.updateLane(this.buildLane(), successCallback)
                :
                this.props.saveNewLane(this.buildLane(), successCallback);
        }
    }

    render() {
        const { flatRate, saveInProgress, startLocationTitle, endLocationTitle, showDeleteModal } = this.state;
        const { showModal, displayContent } = this.props;

        return (
            <Modal
                centered
                open={showModal}
                closeOnEscape={true}
                closeOnDimmerClick={true}
                onClose={() => { this.onClose() }}
                size='small'
            >
                <Modal.Header as={Header} textAlign='center' >{displayContent ? 'Edit Lane' : 'New Lane'} </Modal.Header>

                {displayContent ?
                    <Button style={{ position: 'absolute', top: '0', right: '0', marginRight: '1rem', marginTop: '1rem' }}
                        icon={'trash'}
                        size='large'
                        onClick={() => {
                            this.handleDeleteModal(true);
                        }}
                        content='Delete'
                        negative
                        compact
                    />
                    :
                    null
                }

                <Modal.Content >
                    <Form>
                        <Form.Group widths='equal' >
                            <Form.Field required >
                                <GeoSearchBox
                                    iconState={displayContent ? 2 : 0}
                                    inputValue={startLocationTitle}
                                    isInputValid={(startLaneValid) => { this.setState({ startLaneValid }) }}
                                    handleLocationSelect={(location) => {
                                        this.setState({
                                            startLocationPlaceId: location.place_id,
                                            startLocationTitle: location.description
                                        });
                                    }}
                                    header={"From:"}
                                />
                            </Form.Field>
                            <Form.Field required >
                                <GeoSearchBox
                                    iconState={displayContent ? 2 : 0}
                                    inputValue={endLocationTitle}
                                    isInputValid={(endLaneValid) => { this.setState({ endLaneValid }) }}
                                    handleLocationSelect={(location) => {
                                        this.setState({
                                            endLocationPlaceId: location.place_id,
                                            endLocationTitle: location.description
                                        });
                                    }}
                                    header={"To:"}
                                />
                            </Form.Field>
                        </Form.Group>
                        <Form.Input
                            fluid
                            type='number'
                            id='form-subcomponent-shorthand-input-odot'
                            label='Flatrate'
                            placeholder='flatrate'
                            value={flatRate}
                            onChange={(e) => { this.handFormFieldUpdate('flatRate', e.target.value); }}
                        />
                    </Form>
                </Modal.Content>

                <Modal.Actions>
                    {showDeleteModal ? <DeleteModal
                        entity={'this lane?'}
                        onConfirmDelete={() => {
                            this.props.deleteLane(displayContent.id);
                            this.handleDeleteModal(false);
                            this.props.hideLaneModal();
                        }}
                        onCancel={() => { this.handleDeleteModal(false); }}
                    />
                        :
                        null
                    }
                    <Button onClick={() => { this.onClose() }} >
                        Cancel
                    </Button>
                    <Button
                        onClick={() => { this.onSaveLane(); }}
                        positive
                        labelPosition='right'
                        icon='save'
                        content={displayContent ? 'Save' : 'Create'}
                        loading={saveInProgress}
                        disabled={!this.isLaneValid()}
                    />
                </Modal.Actions>
            </Modal>
        );
    }
}

const mapStateToProps = state => {
    return {
        showModal: state.newLaneModal.display,
        displayContent: state.newLaneModal.content.id ? state.newLaneModal.content : null,
        selectedCarrier: state.selectedCarrier,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        hideLaneModal: () => {
            dispatch(hideLaneModal());
        },
        saveNewLane: (updatedLane, callback) => {
            dispatch(saveLane(updatedLane, callback));
        },
        updateLane: (updatedLane, callback) => {
            dispatch(updateLane(updatedLane, callback));
        },
        deleteLane: (laneId, callback = () => { }) => {
            dispatch(deleteLane(laneId, callback));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LaneModal);
