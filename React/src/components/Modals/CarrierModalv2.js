import validator from 'validator';

import React, { Component } from 'react';
import { Button, Modal, Form, TextArea, Header, Segment, Label } from 'semantic-ui-react'

import DeleteModal from './DeleteModal';

import { saveCarrier, updateCarrier, deleteCarrier } from '../../actions/carrier-action-index';
import { hideNewCarrierModal } from '../../actions/modal-action-index';

import GeoSearchBox from '../SearchPage/components/SearchPanel/GeoSearchBox';

import { connect } from 'react-redux';

export class CarrierModal extends Component {
    constructor(props) {
        super(props);
        const displayContent = props.displayContent || {};

        this.staticTitle = displayContent.title || null;

        this.state = {
            title: displayContent.title || '',
            notes: displayContent.notes || '',
            homeLocation: displayContent.homeLocation || '',
            homeCity: displayContent.homeCity || '',
            mcNum: displayContent.mcNum || '',
            odotNum: displayContent.odotNum || '',
            showDeleteModal: false,

            // TODO: rename to GeoLocation input
            isLocationValid: displayContent.id ? true : false,
            showTitleError: false
        }
    }

    handleDeleteModal(showDeleteModal = false) {
        this.setState({
            showDeleteModal
        });
    }

    handFormFieldUpdate(field, value) {
        this.setState({ [field]: value });
    }

    buildCarrier() {
        const { title, notes, mcNum, odotNum, homeCity, homeLocation } = this.state;
        const displayContent = this.props.displayContent || {};

        const carrierObj = {
            id: displayContent ? displayContent.id : null,
            title: title,
            notes: notes,
            homeCity: homeCity,
            placeId: homeLocation,
            mcNum: mcNum,
            odotNum: odotNum,
            homeLat: displayContent.homeLat || 0,
            homeLong: displayContent.homeLong || 0,
        }

        return carrierObj;
    }

    handleGeolocation(location) {
        this.setState({ homeLocation: location.place_id, homeCity: location.description, isLocationValid: true });
    }

    isFieldValid(field) {
        const fieldValue = this.state[field];
        if (field === 'title' && fieldValue) {
            return (validator.isAlpha(fieldValue.replace(/ /g, '')) && !validator.isEmpty(fieldValue));
        }

        return false;
    }

    isCarrierValid() {
        const carrier = this.buildCarrier();

        if (carrier.placeId === '' && (carrier.homeLat === 0 && carrier.homeLong === 0)) {
            return false;
        }

        return this.state.isLocationValid && this.isFieldValid('title');
    }

    onSaveCarrier() {
        const { displayContent } = this.props;

        if (this.isCarrierValid()) {
            this.setState({ saveInProgress: true })
            const successCallback = () => { this.setState({ saveInProgress: false }); this.onClose() };
            displayContent ?
                this.props.updateCarrier(this.buildCarrier(), successCallback)
                :
                this.props.saveCarrier(this.buildCarrier(), successCallback);
        }
    }

    onClose() {
        this.props.hideCarrierModal();
    }

    render() {
        const { displayContent } = this.props;
        const {
            title,
            notes,
            mcNum,
            odotNum,
            saveInProgress,
            homeCity,
            showDeleteModal,
            showTitleError
        } = this.state;

        return (
            <Modal
                centered
                open={true}
                closeOnEscape={true}
                closeOnDimmerClick={true}
                onClose={() => { this.onClose() }}
                size='small'
            >
                <Modal.Header as={Header} textAlign='center' >
                    {displayContent ? this.staticTitle : 'New Carrier'}

                    {displayContent ?
                        <Button style={{ position: 'absolute', top: '0', right: '0', marginRight: '1rem', marginTop: '1rem' }}
                            icon={'trash'}
                            size='large'
                            onClick={() => {
                                this.handleDeleteModal(true);
                            }}
                            content='Delete'
                            negative
                            compact
                        />
                        :
                        null
                    }
                </Modal.Header>
                <Modal.Content >
                    <Form>
                        <Form.Group widths='equal' >
                            <Form.Field required >
                                <GeoSearchBox
                                    iconState={displayContent ? 2 : 0}
                                    inputValue={homeCity}
                                    isLocationValid={(isLocationValid) => { this.setState({ isLocationValid }) }}
                                    handleLocationSelect={(location) => { this.handleGeolocation(location) }}
                                    header={"Operates from:"}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Segment style={{ paddingLeft: '0', paddingRight: '0' }} basic >
                                    <div>
                                        <Form.Input
                                            fluid
                                            required
                                            id='form-subcomponent-shorthand-input-title'
                                            label='Title'
                                            placeholder='Title'
                                            value={title}
                                            onBlur={() => {
                                                this.setState({
                                                    showTitleError: !this.isFieldValid('title')
                                                })
                                            }}
                                            onChange={(e) => { this.handFormFieldUpdate('title', e.target.value); }}
                                        />
                                        {showTitleError ?
                                            <Label
                                                active={true}
                                                color='red'
                                                content={'Must include a title.'}
                                                pointing='above'
                                                size='small'
                                            />
                                            :
                                            null
                                        }
                                    </div>
                                </Segment>
                            </Form.Field>
                        </Form.Group>
                        <Form.Group widths='equal'>
                            <Form.Input
                                fluid
                                id='form-subcomponent-shorthand-input-mc'
                                label='MC#'
                                placeholder='#'
                                value={mcNum}
                                onChange={(e) => { this.handFormFieldUpdate('mcNum', e.target.value); }}
                            />
                            <Form.Input
                                fluid
                                id='form-subcomponent-shorthand-input-odot'
                                label='ODOT#'
                                placeholder='#'
                                value={odotNum}
                                onChange={(e) => { this.handFormFieldUpdate('odotNum', e.target.value); }}
                            />
                        </Form.Group>
                        <Form.Field
                            id='form-textarea-carrier-notes'
                            control={TextArea}
                            label='Notes'
                            placeholder='notes'
                            value={notes}
                            onChange={(e) => { this.handFormFieldUpdate('notes', e.target.value); }}
                        />
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    {showDeleteModal ? <DeleteModal
                        entity={`'${title}'`}
                        onConfirmDelete={() => {
                            this.props.deleteCarrier(displayContent.id, () => { });
                            this.handleDeleteModal(false);
                            this.props.hideCarrierModal();
                        }}
                        onCancel={() => { this.handleDeleteModal(false); }}
                    />
                        :
                        null
                    }
                    <Button onClick={() => { this.onClose() }} >
                        Cancel
                    </Button>
                    <Button
                        onClick={() => { this.onSaveCarrier(); }}
                        positive
                        labelPosition='right'
                        icon='save'
                        disabled={!this.isCarrierValid()}
                        content={displayContent ? 'Save' : 'Create'}
                        loading={saveInProgress}
                    />
                </Modal.Actions>
            </Modal>
        );
    }
}

const mapStateToProps = state => {
    return {
        showModal: state.newCarrierModal.display,
        displayContent: state.newCarrierModal.content ? state.newCarrierModal.content : null,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        hideCarrierModal: () => {
            dispatch(hideNewCarrierModal());
        },
        saveCarrier: (updatedCarrier, callback) => {
            dispatch(saveCarrier(updatedCarrier, callback));
        },
        updateCarrier: (updatedCarrier, callback) => {
            dispatch(updateCarrier(updatedCarrier, callback));
        },
        deleteCarrier: (carrierId, callback) => {
            dispatch(deleteCarrier(carrierId, callback));
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(CarrierModal);
