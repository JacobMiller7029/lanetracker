import React from 'react';

import { Button, Modal } from 'semantic-ui-react';

export default (props) => {
    return (
        <Modal
            centered
            open={true}
            closeOnEscape={true}
            closeOnDimmerClick={true}
            onClose={props.onCancel}
            size='tiny'
        >
            <Modal.Header>Are you sure?</Modal.Header>
            <Modal.Content>
                <p>This will permenently delete {props.entity}.</p>
            </Modal.Content>
            <Modal.Actions>
                <Button negative onClick={props.onCancel} >No</Button>
                <Button positive onClick={props.onConfirmDelete} icon='checkmark' labelPosition='right' content='Yes' />
            </Modal.Actions>

        </Modal>
    );
}
