import moment from 'moment';
import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { Button, Form, Grid, Header, Segment, Label } from 'semantic-ui-react'
import { loginUser } from '../../actions/auth-action-index';

import statusEnums from '../../res/enums/AuthorizationStatusEnums';
import ForceLogoutErrorModal from '../Modals/ForceLogoutErrorModal';

import { connect } from 'react-redux';

export class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            showErrors: false,
            valid: false,
        }

        const { authToken, history } = this.props;
        const isUserLoggedIn = (authToken.token !== undefined && authToken.token !== null && authToken.token.length > 0);
        const isTokenValid = (authToken.expires && moment().utc().isBefore(authToken.expires));

        if (isUserLoggedIn && isTokenValid) {
            history.push('/search');
        }
    }

    handleFormFieldUpdate(field, value) {
        this.setState({ [field]: value });
    }

    handleSuccessfulLogin() {
        this.props.history.push('/search')
    }

    handleFailedPasswordAttempt() {
        this.setState({ showErrors: true });
    }

    render() {
        const { username, password, showErrors } = this.state;
        const { authToken } = this.props;
        const showLoggedOutModal = authToken.status === statusEnums.FORCED_LOGOUT;

        return (
            <div className='login-form' style={{ height: '100%' }} >
                {showLoggedOutModal ? <ForceLogoutErrorModal /> : null}
                <Grid textAlign='center' style={{ height: '100%' }} verticalAlign='middle'>
                    <Grid.Column style={{ maxWidth: 450 }}>
                        <Header as='h2' textAlign='center'>
                            Login
                        </Header>

                        {showErrors ?
                            <Segment basic >
                                <Label
                                    active={true}
                                    color='red'
                                    content={'Invalid username or password.'}
                                    size='big'
                                />
                            </Segment>
                            :
                            null
                        }


                        <Form size='large'>
                            <Segment stacked>
                                <Form.Input
                                    fluid icon='user'
                                    iconPosition='left'
                                    placeholder='Username'
                                    onChange={(e) => { this.handleFormFieldUpdate('username', e.target.value); }}
                                />
                                <Form.Input
                                    fluid
                                    icon='lock'
                                    iconPosition='left'
                                    placeholder='Password'
                                    type='password'
                                    onChange={(e) => { this.handleFormFieldUpdate('password', e.target.value); }}
                                />

                                <Button
                                    primary
                                    fluid
                                    size='large'
                                    onClick={() => { this.setState({ showErrors: false }); this.props.loginUser(username, password, () => { this.handleSuccessfulLogin() }, () => { this.handleFailedPasswordAttempt() }) }}
                                >
                                    Login
                                </Button>
                            </Segment>
                        </Form>
                    </Grid.Column>
                </Grid>
            </div >
        )
    }
}

const mapStateToProps = state => {
    return {
        authToken: state.authToken,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        loginUser: (username, password, successCallback, failureCallback) => {
            dispatch(loginUser(username, password, successCallback, failureCallback));
        }
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginForm));

