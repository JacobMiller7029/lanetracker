import React from 'react';
import { shallow } from 'enzyme';
import { LoginForm } from './LoginPage';

describe('Tests LoginPage component', () => {
    const history = {
        push: () => {}
    }

    const authToken = {
        token: 'notARealToken',
        expires: '',
    }

    it('LoginPage component renders successfully', () => {
        shallow(<LoginForm authToken history />);
    });    
    
});