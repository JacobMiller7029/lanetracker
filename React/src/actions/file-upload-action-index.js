import * as _ from 'lodash';
import { ExcelRenderer } from 'react-excel-renderer';

import fileUploadApi from '../api/fileupload-api-index'
import { rowToObject, transformEntry } from '../utility/spreadsheetutility'
import { removeAuthToken } from './auth-action-index';

// Functionality is a WIP

export const processCarrierSpreadsheet = (e, dispatch) => {
    const fileObj = e.target.files[0];
    ExcelRenderer(fileObj, (err, resp) => {
        if (err) {
            // TODO: Display Error modal here
            return null;
        } else {
            const formattedLanes = [];

            _.forEach(resp.rows, (row) => {

                const rowObject = rowToObject(row, resp.rows[0]);
                const formattedData = transformEntry(rowObject);

                const uploadObject = {
                    carrierDetails: { title: formattedData.carrier.title },
                    laneDetails: formattedData
                };

                formattedLanes.push(uploadObject);
            });

            uploadSheet(formattedLanes)(dispatch);
        }
    });
}

// TODO: Redo. Backend needs a multi-part file request set up
export const uploadSheet = (sheet) => {
    return(dispatch) => {
        fileUploadApi.uploadCarrierSpreedSheet(sheet)
        .then((response) => {
            console.log('Bulk upload spreadsheet results', response);
            // dispatch(addContactToSelectedCarrier(response.data));
            // successCallback(response.data);
        }).catch((error) => {
            // successCallback(null);
            if (error && error.response.status === 401) {
                dispatch(removeAuthToken());
            }
        })
    }
}

