import laneApi from '../api/lane-api-index'
import { removeAuthToken } from './auth-action-index';
import action_constants from '../res/strings/action_constants';

export const setSearchModeLane = () => {
    return {
        type: action_constants.SEARCH_TYPE_LANE,
    }
}

export const setLaneSearchResults = (laneSearchResults) => {
    return {
        type: action_constants.SEARCH_LANES,
        payload: laneSearchResults
    }
}

export const removeLaneFromSelectedCarrier = (laneId) => {
    return {
        type: action_constants.REMOVE_LANE_FROM_SELECTED_CARRIER,
        payload: laneId
    };
}

export const addLaneToSelectedCarrier = (lane) => {
    return {
        type: action_constants.ADD_LANE_TO_SELECTED_CARRIER,
        payload: lane
    };
}

export const updateLaneInSearchResultsList = (lane) => {
    return {
        type: action_constants.UPDATE_LANE_IN_SEARCH_RESULTS,
        payload: lane
    }
}

export const removeLaneFromSearchResults = (laneId) => {
    return {
        type: action_constants.REMOVE_LANE_FROM_SEARCH_RESULTS,
        payload: laneId
    }
}

export const saveLane = (laneObj, successCallback) => {
    return (dispatch) => {
        laneApi.postLane(laneObj)
            .then((response) => {
                dispatch(addLaneToSelectedCarrier(response.data));
                successCallback(response.data);
            }).catch((error) => {
                successCallback(null);
                if (error && error.response.status === 401) {
                    dispatch(removeAuthToken());
                }
            })
    }
}

export const updateLane = (laneObj, successCallback) => {
    return (dispatch) => {
        laneApi.putLane(laneObj)
            .then((response) => {
                dispatch(addLaneToSelectedCarrier(response.data));
                dispatch(updateLaneInSearchResultsList(response.data));
                successCallback(response.data);
            }).catch((error) => {
                if (error && error.response.status === 401) {
                    dispatch(removeAuthToken());
                }
                successCallback();
            })
    }
}

export const deleteLane = (laneId, successCallback) => {
    return (dispatch) => {
        laneApi.deleteLane(laneId)
            .then((response) => {
                dispatch(removeLaneFromSelectedCarrier(laneId))
                dispatch(removeLaneFromSearchResults(laneId));
                successCallback(laneId);
            }).catch((error) => {
                successCallback();
                if (error && error.response.status === 401) {
                    dispatch(removeAuthToken());
                }
            })
    }
}

export const searchForLane = ({ pickup, dropoff, searchRadius, bidirectional }) => {
    return dispatch => {
        if (pickup && dropoff && searchRadius) {

            dispatch(setSearchModeLane());

            const params = {
                start: pickup,
                end: dropoff,
                radius: searchRadius,
                bidirectional,
            };

            // Set it to empty to trigger the loading animation on the table.
            dispatch(setLaneSearchResults({
                searching: true,
                searchData: []
            }));

            laneApi.queryForLane(params)
                .then(response => {
                    if (response && response.status === 200) {
                        return response.data;
                    }
                }).then(data => {
                    dispatch(setLaneSearchResults({
                        searching: false,
                        searchData: data
                    }));
                }).catch((error) => {
                    dispatch(setLaneSearchResults({
                        searching: false,
                        searchData: []
                    }));
                    if (error && error.response.status === 401) {
                        dispatch(removeAuthToken());
                    }
                });

        } else {
            dispatch(setLaneSearchResults(({
                searching: false,
                searchData: []
            })));
        }
    }
};