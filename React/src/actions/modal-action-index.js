import action_constants from '../res/strings/action_constants';

export const displayLoginErrorModal = () => {
    return {
        type: action_constants.SHOW_LOGIN_ERROR_MODAL,
    }
}

export const hideLoginErrorModal = () => {
    return {
        type: action_constants.HIDE_LOGIN_ERROR_MODAL,
    }
}

export const showForcedLogoutModal = () => {
    return {
        type: action_constants.SHOW_FOCED_LOGOUT_MODAL
    }
}


export const hideForcedLogoutModal = () => {
    return {
        type: action_constants.HIDE_FORCED_LOGOUT_MODAL
    }
}

export const showContactModal = (content = null) => {
    return {
        type: action_constants.SHOW_CONTACT_MODAL,
        payload: {
            display: true,
            content
        }
    };
};

export const hideContactModal = () => {
    return {
        type: action_constants.HIDE_CONTACT_MODAL,
        payload: {
            display: false,
            content: null
        }
    };
};

export const showLaneModal = (content = null) => {
    return {
        type: action_constants.SHOW_LANE_MODAL,
        payload: {
            display: true,
            content
        }
    };
};

export const hideLaneModal = () => {
    return {
        type: action_constants.HIDE_LANE_MODAL,
        payload: {
            display: false,
            content: null
        }
    };
};

export const showCarrierModal = (content = null) => {
    return {
        type: action_constants.SHOW_NEW_CARRIER_MODAL,
        payload: {
            display: true,
            content
        }
    }
}

export const hideNewCarrierModal = () => {
    return {
        type: action_constants.HIDE_NEW_CARRIER_MODAL,
        payload: {
            display: false,
            content: null
        }
    }
}
