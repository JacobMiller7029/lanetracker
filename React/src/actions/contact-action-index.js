import contactApi from '../api/contact-api-index'
import action_constants from '../res/strings/action_constants';
import { removeAuthToken } from './auth-action-index';

export const removeContactFromSelectedCarrier = (contactId) => {
    return {
        type: action_constants.REMOVE_CONTACT_FROM_SELECTED_CARRIER,
        payload: contactId
    };
}

// Add or Update
export const addContactToSelectedCarrier = (contact) => {
    return {
        type: action_constants.ADD_CONTACT_TO_SELECTED_CARRIER,
        payload: contact
    };
}

export const saveContact = (contactObj, successCallback) => {
    return (dispatch) => {
        contactApi.postContact(contactObj)
            .then((response) => {
                dispatch(addContactToSelectedCarrier(response.data));
                successCallback(response.data);
            }).catch((error) => {
                successCallback(null);
                if (error && error.response.status === 401) {
                    dispatch(removeAuthToken());
                }
            })
    }
}

export const updateContact = (contactObj, successCallback) => {
    return (dispatch) => {
        contactApi.putContact(contactObj)
            .then((response) => {
                dispatch(addContactToSelectedCarrier(response.data));
                successCallback(response.data);
            }).catch((error) => {
                successCallback();
                if (error && error.response.status === 401) {
                    dispatch(removeAuthToken());
                }
            })
    }
}

export const deleteContact = (contactId, successCallback) => {
    return (dispatch) => {
        contactApi.deleteContact(contactId)
            .then((response) => {
                dispatch(removeContactFromSelectedCarrier(contactId))
                successCallback(contactId);
            }).catch((error) => {
                successCallback();
                if (error && error.response.status === 401) {
                    dispatch(removeAuthToken());
                }
            })
    }
}
