import authApi from '../api/auth-api-index';
import action_constants from '../res/strings/action_constants';
import statusEnums from '../res/enums/AuthorizationStatusEnums';

export const loadToken = () => {
    const token = localStorage.getItem(action_constants.SAVE_TOKEN)
    return token ? (JSON.parse(token)).token : null;
}

export const loadTokenExperationDate = () => {
    const savedToken = localStorage.getItem(action_constants.SAVE_TOKEN)
    return savedToken ? (JSON.parse(savedToken)).expires : 0;
}

export const setAuthToken = (authResponse) => {
    localStorage.setItem(action_constants.SAVE_TOKEN, JSON.stringify(authResponse));
    return {
        type: action_constants.SET_AUTH_TOKEN,
        payload: { status: statusEnums.HAS_TOKEN, expires: authResponse.expires, token: authResponse.token }
    }
}

export const removeAuthToken = (status = statusEnums.FORCED_LOGOUT) => {
    localStorage.removeItem(action_constants.SAVE_TOKEN);
    return {
        type: action_constants.REMOVE_AUTH_TOKEN,
        payload: status,
    }
}

export const loginUser = (username, password, successCallback, failureCallback) => {
    const payload = {
        username,
        password
    }
    return (dispatch) => {
        authApi.postCredentials(payload)
            .then((response) => {
                if (response.status === 200) {
                    dispatch(setAuthToken(response.data));
                }
                successCallback();
            })
            .catch((error) => {
                failureCallback();
            });
    };
}

export default {
    loadToken,
    loadTokenExperationDate,
    setAuthToken,
    removeAuthToken,
    loginUser
}