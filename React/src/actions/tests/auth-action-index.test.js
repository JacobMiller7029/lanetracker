jest.mock('../../api/auth-api-index');
import authApi from '../../api/auth-api-index';
import { loadToken, loadTokenExperationDate, setAuthToken, removeAuthToken, loginUser } from '../auth-action-index'
import action_constants from '../../res/strings/action_constants';
import statusEnums from '../../res/enums/AuthorizationStatusEnums';

describe('Tests auth-action-index methods', () => {

    beforeEach(() => {
        jest.clearAllMocks()
    })

    it('Tests setAuthToken function', () => {
        const authToken = {
            token: 'AA',
            expires: 10
        }

        jest.spyOn(window.localStorage.__proto__, 'setItem');
        window.localStorage.__proto__.setItem = jest.fn();

        const actionOutput = setAuthToken(authToken)

        expect(localStorage.setItem).toHaveBeenCalled();
        expect(actionOutput.type).toBe(action_constants.SET_AUTH_TOKEN)
        expect(actionOutput.payload).toStrictEqual({ status: statusEnums.HAS_TOKEN, expires: authToken.expires, token: authToken.token })
    })

    describe('Tests loadToken methods', () => {
        it('loadToken returns token after loading it from local storage', () => {
            const token = 'a-token'
            jest.spyOn(window.localStorage.__proto__, 'getItem');
            window.localStorage.__proto__.getItem = jest.fn(() =>  (JSON.stringify({token, expires: '10'})));

            const actionOutput = loadToken()
            expect(localStorage.getItem).toHaveBeenCalled();
            expect(actionOutput).toBe(token)
        });

        it('loadToken returns null when local storage is empty', () => {
            jest.spyOn(window.localStorage.__proto__, 'getItem');
            window.localStorage.__proto__.getItem = jest.fn();

            const actionOutput = loadToken()
            expect(localStorage.getItem).toHaveBeenCalled();
            expect(actionOutput).toBe(null)
        });
    });

    describe('Tests loadTokenExperationDate methods', () => {
        it('loadTokenExperationDate function returns saved experation date', () => {
            const expires = 10
            jest.spyOn(window.localStorage.__proto__, 'getItem');
            window.localStorage.__proto__.getItem = jest.fn(() =>  (JSON.stringify({token: '', expires})));

            const actionOutput = loadTokenExperationDate()
            expect(localStorage.getItem).toHaveBeenCalled();
            expect(actionOutput).toBe(expires)
        })

        it('Tests loadTokenExperationDate function', () => {
                jest.spyOn(window.localStorage.__proto__, 'getItem');
                window.localStorage.__proto__.getItem = jest.fn();

                const actionOutput = loadTokenExperationDate()
                expect(localStorage.getItem).toHaveBeenCalled();
                expect(actionOutput).toBe(0)
        })
    });

    it('Tests removeAuthToken function', () => {
        jest.spyOn(window.localStorage.__proto__, 'removeItem');
        window.localStorage.__proto__.removeItem = jest.fn();

        const actionOutput = removeAuthToken()
        expect(localStorage.removeItem).toHaveBeenCalled();
        expect(actionOutput.type).toBe(action_constants.REMOVE_AUTH_TOKEN)
        expect(actionOutput.payload).toStrictEqual(statusEnums.FORCED_LOGOUT)
    })

    describe('Tests loginUser method - ', () => {
        it('calls dispatch and successCallback on response with status 200', async () => {
            const dispatch = jest.fn();
            const successCallback = jest.fn();
            authApi.postCredentials = jest.fn(() => { return new Promise((resolve) => resolve({status: 200, data: []})) });

            await loginUser('test', 'name', successCallback, null)(dispatch);

            expect(successCallback).toHaveBeenCalled()
            expect(dispatch).toHaveBeenCalled()
            expect(authApi.postCredentials).toHaveBeenCalled();
        });

        it('should not call dispatch if status code is not 200', async () => {
            const dispatch = jest.fn();
            const successCallback = jest.fn();
            authApi.postCredentials = jest.fn(() => { return new Promise((resolve) => resolve({status: 201, data: []})) });

            await loginUser('test', 'name', successCallback, null)(dispatch);

            expect(dispatch).not.toHaveBeenCalled()
            expect(successCallback).toHaveBeenCalled()
            expect(authApi.postCredentials).toHaveBeenCalled();
        });

        it('Should call failureCallback on network failure', async () => {
            const dispatch = jest.fn();
            const failureCallback = jest.fn();
            authApi.postCredentials = jest.fn(() => { return new Promise((resolve, reject) => reject(new Error( 'failure'))) });

            await loginUser('test', 'name', null, failureCallback)(dispatch);

            expect(authApi.postCredentials).toHaveBeenCalled();
            expect(dispatch).not.toHaveBeenCalled()
        })
    });
})