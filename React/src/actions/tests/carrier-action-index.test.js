jest.mock('../../api/carrier-api-index');
import carrierApi from '../../api/carrier-api-index'
import { 
    setSearchModeCarrier, 
    setCarrierSearchResults, 
    updateSelectedCarrier, 
    showSelectedCarrier, 
    hideSelectedCarrier, 
    updateCarrierInSearchResultsList, 
    removeCarrierFromSearchResults, 
    getCarrierById,
    saveCarrier, 
    updateCarrier, 
    deleteCarrier, 
    searchForCarrier 
} from '../carrier-action-index'
import action_constants from '../../res/strings/action_constants';
import { doesNotReject } from 'assert';

describe('Tests auth-action-index methods', () => {

    describe('dispatch Actions - ', () => {
        it('setSearchModeCarrier', () => {
            const actionResult = setSearchModeCarrier();
            expect(actionResult.type).toBe(action_constants.SEARCH_TYPE_CARRIER)
        })

        it('setCarrierSearchResults', () => {
            const items = ['item']
            const actionResult = setCarrierSearchResults(items);
            expect(actionResult.type).toBe(action_constants.SEARCH_CARRIER)
            expect(actionResult.payload).toBe(items)
        })

        it('updateSelectedCarrier', () => {
            const items = {title: 'title'}
            const actionResult = updateSelectedCarrier(items);
            expect(actionResult.type).toBe(action_constants.UPDATE_SELECTED_CARRIER)
            expect(actionResult.payload).toBe(items)
        })

        it('showSelectedCarrier', () => {
            const actionResult = showSelectedCarrier();
            expect(actionResult.type).toBe(action_constants.DISPLAY_SELECTED_CARRIER)
            expect(actionResult.payload).toBe(true)
        })

        it('hideSelectedCarrier', () => {
            const actionResult = hideSelectedCarrier();
            expect(actionResult.type).toBe(action_constants.DISPLAY_SELECTED_CARRIER)
            expect(actionResult.payload).toBe(false)
        })

        it('updateCarrierInSearchResultsList', () => {
            const carrier = {title: 'title'}
            const actionResult = updateCarrierInSearchResultsList(carrier);
            expect(actionResult.type).toBe(action_constants.UPDATE_CARRIER_IN_SEARCH_RESULTS)
            expect(actionResult.payload).toBe(carrier)
        })

        it('removeCarrierFromSearchResults', () => {
            const carrierId = 10
            const actionResult = removeCarrierFromSearchResults(carrierId);
            expect(actionResult.type).toBe(action_constants.REMOVE_CARRIER_FROM_SEARCH_RESULTS)
            expect(actionResult.payload).toBe(carrierId)
        })
    });

    describe('API Request and results - ', () => {
        describe('getCarrierById - ', () => {
            it('getCarrierById calls dispatch and callback on success', async () => {
                const dispatch = jest.fn();
                const callback = jest.fn();
                carrierApi.getCarrierById = jest.fn(() => { return new Promise((resolve) => resolve({status: 200, data: []})) });
   
                await getCarrierById(10, callback)(dispatch);

                expect(carrierApi.getCarrierById).toHaveBeenCalled();
                expect(dispatch).toHaveBeenCalled()
                expect(callback).toHaveBeenCalled()
            })

            it('getCarrierById callback not called on failure', async () => {
                const dispatch = jest.fn();
                const callback = jest.fn();
                carrierApi.getCarrierById = jest.fn(() => { return new Promise((resolve, reject) => reject({response: { status: 401 } })) });
   
                await getCarrierById(10, callback)(dispatch);

                expect(callback).not.toHaveBeenCalled()
                expect(carrierApi.getCarrierById).toHaveBeenCalled();
            })
        });

        describe('saveCarrier - ', () => {
            // it('saveCarrier calls dispatch and callback on success', async (done) => {
            //     expect.assertions(3)

            //     const dispatch = jest.fn();
            //     const callback = jest.fn();
            //     carrierApi.postCarrier = jest.fn(() => { return new Promise((resolve) => resolve({status: 200, data: []})) });
   
            //     const networkcall = await saveCarrier({}, callback)(dispatch);
            //     console.log('nwcall', networkcall)

            //     console.log('ASSERTIONS');
            //     expect(carrierApi.postCarrier).toHaveBeenCalled();
            //     expect(dispatch).toHaveBeenCalledTimes(2)
            //     expect(callback).toHaveBeenCalled()
            // })

            it('saveCarrier callback not called on failure', async () => {
                const dispatch = jest.fn();
                const callback = jest.fn();
                carrierApi.postCarrier = jest.fn( async () => { return await new Promise((resolve, reject) => reject({response: { status: 401 } })) });
   
                console.log('awating save carrier function call');
                await saveCarrier(10, callback)(dispatch);
                console.log('await finished');

                console.log('ASSERTIONS')
                expect(callback).not.toHaveBeenCalled();
                expect(dispatch).toHaveBeenCalled();
                expect(carrierApi.postCarrier).toHaveBeenCalled();
            })
        });

        // describe('saveCarrier - ', () => {
        //     it('saveCarrier calls dispatch and callback on success', async () => {
        //         const dispatch = jest.fn();
        //         const callback = jest.fn();
        //         carrierApi.postCarrier = jest.fn(() => { return new Promise((resolve) => resolve({status: 200, data: []})) });
   
        //         await saveCarrier({}, callback)(dispatch);

        //         expect(carrierApi.postCarrier).toHaveBeenCalled();
        //         expect(dispatch).toHaveBeenCalledTimes(2)
        //         expect(callback).toHaveBeenCalled()
        //     })

        //     it('saveCarrier callback not called on failure', async () => {
        //         const dispatch = jest.fn(() => { console.log('Dispatch called!')});
        //         const callback = jest.fn((arg) => {  });

        //         carrierApi.postCarrier = jest.fn(() => { return new Promise((resolve, reject) => reject({response: { status: 401 } })) });
   
        //         await saveCarrier(10, callback)(dispatch);

        //         // expect(dispatch).toHaveBeenCalled();
        //         expect(carrierApi.postCarrier).toHaveBeenCalled();
        //     })
        // });

    });
});