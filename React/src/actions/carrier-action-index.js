import carrierApi from '../api/carrier-api-index'
import { removeAuthToken } from './auth-action-index';
import action_constants from '../res/strings/action_constants';


export const setSearchModeCarrier = () => {
    return {
        type: action_constants.SEARCH_TYPE_CARRIER,
    }
}

export const setCarrierSearchResults = (carrierSearchResults) => {
    return {
        type: action_constants.SEARCH_CARRIER,
        payload: carrierSearchResults
    }
}

export const updateSelectedCarrier = carrierObj => {
    return {
        type: action_constants.UPDATE_SELECTED_CARRIER,
        payload: carrierObj
    };
};

export const showSelectedCarrier = () => {
    return {
        type: action_constants.DISPLAY_SELECTED_CARRIER,
        payload: true
    };
};

export const hideSelectedCarrier = () => {
    return {
        type: action_constants.DISPLAY_SELECTED_CARRIER,
        payload: false
    };
}

export const updateCarrierInSearchResultsList = (carrier) => {
    return {
        type: action_constants.UPDATE_CARRIER_IN_SEARCH_RESULTS,
        payload: carrier
    }
}

export const removeCarrierFromSearchResults = (carrierId) => {
    return {
        type: action_constants.REMOVE_CARRIER_FROM_SEARCH_RESULTS,
        payload: carrierId
    }
}

export const getCarrierById = (id, callback) => {
     return (dispatch) => {
        carrierApi.getCarrierById(id)
            .then((response => {
                dispatch(updateSelectedCarrier(response.data));
                callback();
            })).catch((error) => {
                if (error && error.response.status === 401) {
                    dispatch(removeAuthToken());
                }
            })
    }
};

export const saveCarrier = (carrierObj, callback) => {
    return (dispatch) => {
        carrierApi.postCarrier(carrierObj)
            .then((response) => {
                console.log('.then');
                dispatch(updateSelectedCarrier(response.data));
                dispatch(showSelectedCarrier(response.data));
            }).catch((error) => {
                console.log('.catch')
                if (error && error.response.status === 401) {
                    dispatch(removeAuthToken());
                }
            })
            .finally(() => {
                console.log('Finally!');
                callback();
            })
    }
}

export const updateCarrier = (carrierObj, successCallback) => {
    return (dispatch) => {
        carrierApi.putCarrier(carrierObj)
            .then((response) => {
                dispatch(updateSelectedCarrier(response.data));
                dispatch(updateCarrierInSearchResultsList(response.data));
                successCallback();
            }).catch((error) => {
                if (error && error.response.status === 401) {
                    dispatch(removeAuthToken());
                }
                successCallback();
            })
    }
}

export const deleteCarrier = (carrierId, successCallback) => {
    return (dispatch) => {
        carrierApi.deleteCarrier(carrierId)
            .then((response) => {
                dispatch(hideSelectedCarrier())
                dispatch(updateSelectedCarrier(null));
                dispatch(removeCarrierFromSearchResults(carrierId));
                successCallback();
            }).catch((error) => {
                if (error && error.response.status === 401) {
                    dispatch(removeAuthToken());
                }
                successCallback();
            })
    }
}

export const searchForCarrier = (type, query) => {
    return dispatch => {

        // Set it to empty to trigger the loading animation on the table.
        dispatch(setCarrierSearchResults({
            searching: true,
            searchData: []
        }));

        dispatch(setSearchModeCarrier());
        carrierApi.queryForCarrier(type, query)
            .then(response => {
                if (response && response.status === 200) {
                    return response.data;
                }
                dispatch(setCarrierSearchResults({
                    searching: false,
                    searchData: [],
                }));
            }).then(data => {
                dispatch(setCarrierSearchResults({
                    searching: false,
                    searchData: data
                }));
            }).catch((error) => {
                if (error.response && error.response.status === 401) {
                    dispatch(removeAuthToken());
                }
                dispatch(setCarrierSearchResults({
                    searching: false,
                    searchData: []
                }));
            });
    }
};
