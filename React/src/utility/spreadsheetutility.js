//  ['aplinehaul', 'carrier', 'consigcty', 'consigst', 'shippercity', 'shipperst', 'eq', 'mode'];

export const rowToObject = (row, headers) => {
    const rowObject = {};

    row.forEach((value, i) => {
        const key = headers[i].replace(/\W/g, '').replace(/ /g, '').toLowerCase();
        rowObject[key] = value;
    });

    return rowObject;
}

export const transformEntry = (spreadSheetEntry) => {
    const transformedEntry = {};

    transformedEntry.carrier = { title: spreadSheetEntry.carrier };
    transformedEntry.flatRate = spreadSheetEntry.aplinehaul;
    transformedEntry.equipment = spreadSheetEntry.eq;
    transformedEntry.mode = spreadSheetEntry.mode;
    transformedEntry.startLocationTitle = spreadSheetEntry.shippercity + ', ' + spreadSheetEntry.shipperst;
    transformedEntry.endLocationTitle = spreadSheetEntry.consigcty + ', ' + spreadSheetEntry.consigst;

    return transformedEntry;
}


// Error handling will be the only real challenge... I suppose i could create a list of all failed entries
// report them to the user. User could download the list for fixing it, or manually add entries.
// Will need a modal to display to the user that an action is in progress...
// 