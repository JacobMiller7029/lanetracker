import axios from 'axios';
import api_strings from '../res/strings/api_strings';
import { loadToken } from '../actions/auth-action-index';

const carrierUrl = `${api_strings.ROOT_URL}${api_strings.CARRIER_ENDPOINT}`;

export const getCarrierById = (id) => {
    const getUrl = `${carrierUrl}${'/' + id}`;
    return axios.get(getUrl, { headers: { Authorization: 'Bearer ' + loadToken() } })
}

export const postCarrier = (carrierObj) => {
    return axios.post(`${carrierUrl}`, [carrierObj], { headers: { Authorization: 'Bearer ' + loadToken() } })
}

export const putCarrier = (carrierObj) => {
    return axios.put(`${carrierUrl}${'/' + carrierObj.id}`, carrierObj, { headers: { Authorization: 'Bearer ' + loadToken() } })
}

export const deleteCarrier = (carrierId) => {
    return axios.delete(`${carrierUrl + '/' + carrierId}`, { headers: { Authorization: 'Bearer ' + loadToken() } })
}

export const queryForCarrier = (type, query) => {
    const searchUrl = `${carrierUrl}?type=${type}&query=${query}`.replace('#', '%23');
    const extras = { headers: { Authorization: 'Bearer ' + loadToken() } };
    return axios.get(searchUrl, extras)
}

export default {
    getCarrierById,
    postCarrier,
    putCarrier,
    deleteCarrier,
    queryForCarrier
}