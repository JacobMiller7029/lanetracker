import axios from 'axios';
import api_strings from '../res/strings/api_strings';
import { loadToken } from '../actions/auth-action-index';

const bulkUploadEndpoint = `${api_strings.ROOT_URL}${api_strings.BULK_UPLOAD_ENDPOINT}`;

export const uploadCarrierSpreedSheet = (sheet) => {
    axios.post(`${bulkUploadEndpoint}`, sheet, { headers: { Authorization: 'Bearer ' + loadToken() } })
}

export default {
    uploadCarrierSpreedSheet,
}