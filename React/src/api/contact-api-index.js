import axios from 'axios';
import api_strings from '../res/strings/api_strings';
import { loadToken } from '../actions/auth-action-index';

const contactUrl = `${api_strings.ROOT_URL}${api_strings.CONTACT_ENDPOINT}`;

export const postContact = (contactObj) => {
    return axios.post(`${contactUrl}`, [contactObj], { headers: { Authorization: 'Bearer ' + loadToken() } })
}

export const putContact = (contactObj) => {
    return axios.put(contactUrl + '/' + contactObj.id, contactObj, { headers: { Authorization: 'Bearer ' + loadToken() } })
}

export const deleteContact = (contactId) => {
    return axios.delete(`${contactUrl + '/' + contactId}`, { headers: { Authorization: 'Bearer ' + loadToken() } })
}

export default {
    postContact,
    putContact,
    deleteContact
}