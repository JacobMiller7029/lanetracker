import axios from 'axios';
import api_strings from '../res/strings/api_strings';
import constants from '../res/strings/action_constants';
import { loadToken } from '../actions/auth-action-index';

const BASE_URL = api_strings.ROOT_URL + '/google';
const headers = {
    "Access-Control-Allow-Origin": "*",
    "Accept": "application/json; charset=UTF-8",
    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
    "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization, application/json",
};

export default class GooglePlacesApi {
    constructor(config) {
        this.config = config;
        this.cancelToken = null;
    }

    queryForPlace(place) {
        if (this.cancelToken !== null) {
            this.cancelToken.cancel(constants.CANCELED_SEARCH);
        }

        const CancelToken = axios.CancelToken;
        this.cancelToken = CancelToken.source();

        const fullUrl = `${BASE_URL}?input=${place}`;

        return axios.get(fullUrl, {
            headers: {
                ...headers,
                Authorization: 'Bearer ' + loadToken(),
            },
            cancelToken: this.cancelToken.token,
        });
    }
}