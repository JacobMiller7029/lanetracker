import axios from 'axios';
import api_strings from '../res/strings/api_strings';
import { loadToken } from '../actions/auth-action-index';

const laneUrl = `${api_strings.ROOT_URL}${api_strings.LANE_ENDPOINT}`;

export const queryForLane = (params) => {
    return axios.get(`${laneUrl}`, { params, ...{ headers: { Authorization: 'Bearer ' + loadToken() }} })
}

export const putLane = (laneObj) => {
    return axios.put(`${laneUrl}${'/' + laneObj.id}`, laneObj, { headers: { Authorization: 'Bearer ' + loadToken() } })
}

export const postLane = (laneObj) => {
    return axios.post(`${laneUrl}`, [laneObj], { headers: { Authorization: 'Bearer ' + loadToken() } })
}

export const deleteLane = (laneId) => {
    axios.delete(`${laneUrl + '/' + laneId}`, { headers: { Authorization: 'Bearer ' + loadToken() } })
}

export default {
    queryForLane,
    postLane,
    putLane,
    deleteLane
}

