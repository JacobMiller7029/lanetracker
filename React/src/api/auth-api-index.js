import axios from 'axios';
import api_strings from '../res/strings/api_strings';

const authUrl = `${api_strings.ROOT_URL}${api_strings.AUTH_ENDPOINT}`;

export const postCredentials = async (payload) => {
    console.log('awaiting')
    return await axios.post(authUrl, payload)
}

export default {
    postCredentials
}