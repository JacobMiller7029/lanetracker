import moment from 'moment';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Route, HashRouter, Redirect } from 'react-router-dom';

import Search from './components/SearchPage/SearchPage';
import LoginPage from './components/LoginPage/LoginPage';

export class App extends Component {
    render() {
        const { authToken } = this.props;

        const userHasToken = (authToken.token !== undefined && authToken.token !== null && authToken.token.length > 0);
        const isTokenValid = (authToken.expires && moment().utc().isBefore(authToken.expires));
        const isUserLoggedIn = userHasToken && isTokenValid;

        return (
            <div className="d-flex flex-column h-100">
                <div className="d-flex flex-column h-100">
                    <HashRouter>
                        <Switch>
                            <Route exact path='/login' component={LoginPage} />
                            <PrivateRoute authed={isUserLoggedIn} path='/search' component={Search} />
                            <Route render={() => <Redirect to="/login" />} />
                        </Switch>
                    </HashRouter>
                </div>
            </div>
        );
    }
}

export const PrivateRoute = ({ component: Component, authed, ...rest }) => {
    return (
        <Route
            {...rest}
            render={(props) => authed === true
                ? <Component {...props} />
                : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />}
        />
    )
}

const mapStateToProps = state => {
    return {
        authToken: state.authToken
    }
}

export default connect(mapStateToProps)(App);
