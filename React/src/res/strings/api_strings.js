export default {
    ROOT_URL: process.env.NODE_ENV === 'production' ? 'http://api.lanetracking.org' : 'http://localhost:8090',
    AUTH_ENDPOINT: '/login',
    CARRIER_ENDPOINT: '/carrier',
    LANE_ENDPOINT: '/lanes',
    CONTACT_ENDPOINT: '/contacts',
    BULK_UPLOAD_ENDPOINT: '/bulkupload'
}