export default {
    SEARCH_LANE: 'SEARCH_LANE',
    SEARCH_CARRIER: 'SEARCH_CARRIER',

    SEARCH_MC: 'MC#',
    SEARCH_NAME: 'Name',
    SEARCH_ODOT: 'ODOT#',
} 